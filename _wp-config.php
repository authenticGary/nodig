<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'nodig');

/** MySQL database username */
define('DB_USER', 'gary');

/** MySQL database password */
define('DB_PASSWORD', 'gary');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'EWqm0%VB.?hGq%+cd<%?`@n`vl=.YmPK]MmL&p%6DHe?x4z3O5O)}nQHGcmOs0?v');
define('SECURE_AUTH_KEY',  'vR~Z0VG9hO)d;8/qjtC{]4)pZ|j,8s66_]&?dP-CJa ^[e%UC}gMP6}W,CIggpei');
define('LOGGED_IN_KEY',    'knHNjJRtD~1O,DImgI[<PaZe/6oCePnanZ=S&z>se Cu0/=DEPI.70SxiGfz%.hI');
define('NONCE_KEY',        'q?^6E.R|K6lS=$S.s2W.G=SNQa}(<*(eY8@7:e ,)SipK}qZ[ f;G.;EAvqKzTY+');
define('AUTH_SALT',        'v^jl1<G^gu,>,p8v:UY=[47wGwpIoLds-c@akMN+zl.{zj=#%UEe7mheC*.}97l#');
define('SECURE_AUTH_SALT', 'k-av` a}Cq/x6#8Qt[^RtdpaW;dmrE)~aon&lfp=PvCF6o=cdeu2@[*a+-jQTTVp');
define('LOGGED_IN_SALT',   '_E2gL^Y{`yh_o0&@gHm-%39;L@=MS<iI6HMDrU$hXrxqBwn^OhCP]}>+]spO6o=_');
define('NONCE_SALT',       '<lyBn4|.LQl~aI~~!yam6$4sp{EaZh0!GBu<Y!JZbj.XP@=]OmCd/^zE{.9p=IeW');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'nodig_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
