<?php
/**
 * First class rate.
 *
 * @package WC_RoyalMail/Rate
 */

/**
 * RoyalMail_Rate_First_Class class.
 *
 * Updated on 03/27/2018 as per https://www.royalmail.com/sites/default/files/Our-prices-2018-effective-26-March-2018.pdf.
 * See UK Standard page 6.
 */
class RoyalMail_Rate_First_Class extends RoyalMail_Rate {

	/**
	 * ID/Name of rate.
	 *
	 * @var string
	 */
	protected $rate_id = 'first_class';

	/**
	 * Pricing bands
	 *
	 * Key is size (e.g. 'letter') and value is an array where key is weight in
	 * gram and value is the price (in penny).
	 *
	 * @var array
	 */
	protected $bands = array(
		'letter' => array(
			100 => 67,
		),
		'large-letter' => array(
			100 => 101,
			250 => 140,
			500 => 187,
			750 => 260,
		),
		'small-parcel-wide' => array(
			1000 => 345,
			2000 => 550,
		),
		'small-parcel-deep' => array(
			1000 => 345,
			2000 => 550,
		),
		'small-parcel-bigger' => array(
			1000 => 345,
			2000 => 550,
		),
		'medium-parcel' => array(
			1000  => 575,
			2000  => 895,
			5000  => 1585,
			10000 => 2190,
			20000 => 3340,
		),
	);

	/**
	 * Shipping boxes.
	 *
	 * @var array
	 */
	protected $boxes = array(
		'letter' => array(
			'length'   => 240, // Max L in mm.
			'width'    => 165, // Max W in mm.
			'height'   => 5,   // Max H in mm.
			'weight'   => 100, // Max Weight in grams.
		),
		'large-letter' => array(
			'length'   => 353,
			'width'    => 250,
			'height'   => 25,
			'weight'   => 750,
		),
		'small-parcel-wide' => array(
			'length' => 450,
			'width'  => 350,
			'height' => 80,
			'weight' => 2000,
		),
		'small-parcel-deep' => array(
			'length' => 350,
			'width'  => 250,
			'height' => 160,
			'weight' => 2000,
		),
		'small-parcel-bigger' => array(
			'length' => 450,
			'width'  => 350,
			'height' => 160,
			'weight' => 2000,
		),
		'medium-parcel' => array(
			'length'   => 610,
			'width'    => 460,
			'height'   => 460,
			'weight'   => 20000,
		),
	);

	/**
	 * Cost for signed for delivery.
	 *
	 * @var integer
	 */
	private $signed_for_cost = 110;

	/**
	 * Get quotes for this rate.
	 *
	 * @param  array  $items to be shipped.
	 * @param  string $packing_method the method selected.
	 * @param  string $destination Address to ship to.
	 * @return array
	 */
	public function get_quotes( $items, $packing_method, $destination ) {
		$class_quote    = 0;
		$recorded_quote = 0;
		$packages       = $this->get_packages( $items, $packing_method );

		if ( $packages ) {
			foreach ( $packages as $package ) {

				$quote = 0;

				if ( ! $this->get_rate_bands( $package->id ) ) {
					return false; // Unpacked item.
				}

				$bands = $this->get_rate_bands( $package->id );
				$matched = false;

				$matched = false;
				foreach ( $bands as $band => $value ) {
					if ( is_numeric( $band ) && $package->weight <= $band ) {
						$quote += $value;
						$matched = true;
						break;
					}
				}

				if ( ! $matched ) {
					return null;
				}

				$class_quote    += $quote;
				$recorded_quote += $quote + $this->signed_for_cost;
			}
		}

		// Return pounds.
		$quotes                       = array();
		$quotes['first-class']        = $class_quote / 100;
		$quotes['first-class-signed'] = $recorded_quote / 100;

		return $quotes;
	}
}
