<?php
/**
 * Second class rate.
 *
 * @package WC_RoyalMail/Rate
 */

/**
 * RoyalMail_Rate_Second_Class class.
 *
 * Updated on 03/27/2018 as per https://www.royalmail.com/sites/default/files/Our-prices-2018-effective-26-March-2018.pdf.
 * See UK Standard page 6.
 */
class RoyalMail_Rate_Second_Class extends RoyalMail_Rate {

	/**
	 * ID/Name of rate.
	 *
	 * @var string
	 */
	protected $rate_id = 'second_class';

	/**
	 * Pricing bands
	 *
	 * Key is size (e.g. 'letter') and value is an array where key is weight in
	 * gram and value is the price (in penny).
	 *
	 * @var array
	 */
	protected $bands = array(
		'letter' => array(
			100 => 58,
		),
		'large-letter' => array(
			100 => 79,
			250 => 126,
			500 => 164,
			750 => 222,
		),
		'small-parcel-wide' => array(
			1000 => 295,
			2000 => 295,
		),
		'small-parcel-deep' => array(
			1000 => 295,
			2000 => 295,
		),
		'small-parcel-bigger' => array(
			1000 => 295,
			2000 => 295,
		),
		'medium-parcel' => array(
			1000  => 505,
			2000  => 505,
			5000  => 1375,
			10000 => 2025,
			20000 => 2855,
		),
	);

	/**
	 * Shipping boxes.
	 *
	 * @var array $boxes
	 */
	protected $boxes = array(
		'letter' => array(
			'length'   => 240, // Max L in mm.
			'width'    => 165, // Max W in mm.
			'height'   => 5,   // Max H in mm.
			'weight'   => 100, // Max Weight in grams.
		),
		'large-letter' => array(
			'length'   => 353,
			'width'    => 250,
			'height'   => 25,
			'weight'   => 750,
		),
		'small-parcel-wide' => array(
			'length' => 450,
			'width'  => 350,
			'height' => 80,
			'weight' => 2000,
		),
		'small-parcel-deep' => array(
			'length' => 350,
			'width'  => 250,
			'height' => 160,
			'weight' => 2000,
		),
		'small-parcel-bigger' => array(
			'length' => 450,
			'width'  => 350,
			'height' => 160,
			'weight' => 2000,
		),
		'medium-parcel' => array(
			'length'   => 610,
			'width'    => 460,
			'height'   => 460,
			'weight'   => 20000,
		),
	);

	/**
	 * Cost for signed for delivery.
	 *
	 * @var string
	 */
	private $signed_for_cost = '110';

	/**
	 * Get quotes for this rate.
	 *
	 * @param array  $items to be shipped.
	 * @param string $packing_method selected.
	 * @param string $destination address.
	 *
	 * @return array
	 */
	public function get_quotes( $items, $packing_method, $destination ) {
		$class_quote    = false;
		$recorded_quote = false;
		$packages       = $this->get_packages( $items, $packing_method );

		if ( $packages ) {
			foreach ( $packages as $package ) {

				$quote = 0;

				if ( ! $this->get_rate_bands( $package->id ) ) {
					return false; // Unpacked item.
				}

				$bands = $this->get_rate_bands( $package->id );

				$matched = false;

				foreach ( $bands as $band => $value ) {
					if ( is_numeric( $band ) && $package->weight <= $band ) {
						$quote += $value;
						$matched = true;
						break;
					}
				}

				if ( ! $matched ) {
					return;
				}

				$class_quote    += $quote;
				$recorded_quote += $quote + $this->signed_for_cost;
			}
		}

		// Return pounds.
		$quotes                        = array();
		$quotes['second-class']        = $class_quote / 100;
		$quotes['second-class-signed'] = $recorded_quote / 100;

		return $quotes;
	}
}
