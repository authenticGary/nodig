<?php
/**
* Template part for displaying page content in page.php
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/
*
* @package No_Dig
*/
?>

<div class="container-fluid">
  <div class="row">
    <div class="col header_image" style="padding: 0px;">
      <?php
      $default_header_image = get_field('default_header_image', 'option');
      if (!empty($default_header_image)) { ?>
      <img src="<?php the_field('default_header_image', 'option');?>" alt="">
      <?php   }
      ?>
      
    </div>
  </div>
</div>
<div class="container">
  <div class="row outside">
    <div class="col-md-8 as-content-area as-contact-content"><!-- content holder and flexible content -->
    <div class="col-md-12 as-post-thumbnail">
       <?php if ( has_post_thumbnail() ) {
           the_post_thumbnail('full');
         } else { ?>
     <img src="<?php bloginfo('template_directory'); ?>/assets/images/default-image.jpg" alt="<?php the_title(); ?>" style="border-top-left-radius: 8px; border-top-right-radius: 8px;"/>
      <?php } ?>
    </div>
    <!-- Title -->
    <div class="col-md-12 as-content-title">
      <h2><?php the_title();?></h2>
    </div>
    <!-- Content -->
    <div class="col-md-12 as-content-section">
      <?php the_content();?>
    </div>


<div class="col-md-12 as-comments">
  <?php
  // If comments are open or we have at least one comment, load up the comment template.
        if ( comments_open() || get_comments_number() ) :
          comments_template();
        endif;
  ?>
</div>


    
  </div>
  <div class="col-md-4 stikey_sidebar as-related-posts" id="sticky-sidebar"><!-- floating side bar -->
    <h2>Related Posts</h2>
<!--Start Related Posts-->
<?php
// Build our basic custom query arguments
$custom_query_args = array( 
  'posts_per_page' => 8, // Number of related posts to display
  'post__not_in' => array($post->ID), // Ensure that the current post is not displayed
  'orderby' => 'rand', // Randomize the results
);
// Initiate the custom query
$custom_query = new WP_Query( $custom_query_args );

// Run the loop and output data for the results
if ( $custom_query->have_posts() ) : ?>
  <?php while ( $custom_query->have_posts() ) : $custom_query->the_post(); ?>
    <a href="<?php the_permalink(); ?>"><h5><?php the_title(); ?></h5></a>
  <?php endwhile; ?>
  <?php else : ?>
    <p>Sorry, no related articles to display.</p>
  <?php endif;
// Reset postdata
wp_reset_postdata();
?>
<!--End Related Posts-->
  
</div>
</div>
</div>


	
				

