<?php $season = get_field('season'); 

                    switch ($season ) {
                    	case 'Summer':
                    	    $seasons_image = get_field('summer_image');
                    	    $seasons_icon = get_field('summer_cta_icon');                    		
                    		$seasons_cta_title = get_field('summer_cta_title');
                    		$seasons_text = get_field('summer_cta_text');
                    		break;

                    	case 'Winter':
                    	    $seasons_image = get_field('winter_image');
                    	    $seasons_icon = get_field('winter_cta_icon');                    		
                    		$seasons_cta_title = get_field('winter_cta_title');
                    		$seasons_text = get_field('winter_cta_text');
                    		break;	
                    	
                    	case 'Autumn':
                    	    $seasons_image = get_field('autumn_image');
                    	    $seasons_icon = get_field('autumn_cta_icon');                    		
                    		$seasons_cta_title = get_field('autumn_cta_title');
                    		$seasons_text = get_field('autumn_cta_text');
                    		break;

                       	case 'Spring':
                    	    $seasons_image = get_field('spring_image');
                    	    $seasons_icon = get_field('spring_cta_icon');                    		
                    		$seasons_cta_title = get_field('spring_cta_title');
                    		$seasons_text = get_field('spring_cta_text');
                    		break;
                    			
                    	default:
                    	    $seasons_image = get_field('summer_image');
                    	    $seasons_icon = get_field('summer_cta_icon');                    		
                    		$seasons_cta_title = get_field('summer_cta_title');
                    		$seasons_text = get_field('summer_cta_text');
                    		break;
                    }
?>
<div class="col-md-12 as-season-left">
	<div class="row">
		<img src="<?php echo $seasons_image;?>">
		<div class="col-md-12 cta-seasons">
		<div class="row h-100">
			<div class="col-md-3 my-auto the-season-icon">
				<!-- i class="fas fa-users"></i -->
				<i class="<?php echo $seasons_icon;?>"></i>
			</div>
			<div class="col-md-8">
				<h4><?php  echo $seasons_cta_title;?></h4>
				<?php  echo $seasons_text;?>
				
			</div>
			<div class="col-md-1 my-auto as-season-link">
				<a href="<?php echo site_url();?>/category/<?php echo strtolower($season);?>"><span class="align-middle"><i class="fas fa-angle-right"></i></span></a>
			</div>
		</div>
	</div>	
		
	
	</div>
</div>
