				<header id="masthead" class="site-header">
					<div class="container">
						<div class="row">
							<div class="col-6">
								<div class="site-branding">
									<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><i class="fas fa-bars"></i></button>
									<a href="javascrip:void(0)" data-toggle="modal" data-target="#exampleModalCenter"><i class="fas fa-search"></i></a>
									
									</div><!-- .site-branding -->
									</div>
									<div class="col-6 header_right pull-right text-right">
										<a href="<?php the_field('user_login', 'option');?>"><i class="far fa-user"></i>&nbsp;
										<a href="<?php echo wc_get_cart_url(); ?>"><i class="fas fa-shopping-cart"></i><span class="basket-items-count"><?php echo sprintf ( _n( '%d', '%d', WC()->cart->get_cart_contents_count() ), WC()->cart->get_cart_contents_count() ); ?></span></a>		

									</div>
									</div><!-- #Row -->



						    <div class="row header_bottom_mobile"> <!-- Menu row -->
							
					
							<div class="col">

								    <?php
									the_custom_logo();?>
									
									<nav id="site-navigation" class="main-navigation h-100">
										<a class="menu-toggle as-opened" aria-controls="primary-menu" aria-expanded="false">X</a>
										<div class="col-md-12 mobile_story">
										<a href="<?php the_field('charles_story', 'option');?>">Charles' Story</a>
										</div>
										
										<?php
										wp_nav_menu( array(
											'theme_location' => 'menu-1',
											'menu_id'        => 'primary-menu',
										) );
										?>
										<div class="row menu-buttons-icons">
											<div class="col-md-12 header_right_button header_right_mobile">
												<a href="<?php the_field('start_here_button', 'option');?>" class="btn btn-primary">START HERE</a>
											</div>
										<div class="col-md-12 mobile_icons header_left header-icons">
											<a href="<?php the_field('twitter', 'option');?>" target="_blank"><i class="fab fa-twitter"></i></a>&nbsp;
											<a href="<?php the_field('youtube', 'option');?>" target="_blank"><i class="fab fa-youtube"></i></a>&nbsp;
											<a href="<?php the_field('facebook', 'option');?>" target="_blank"><i class="fa fa-facebook-f"></i></a>&nbsp;
											<a href="<?php the_field('instagram', 'option');?>" target="_blank"><i class="fab fa-instagram"></i></a>
										</div>
										</div>

										</nav><!-- #site-navigation -->
									</div>
									
									</div><!-- #Row -->


									</div><!-- #Container -->
									</header><!-- #masthead -->