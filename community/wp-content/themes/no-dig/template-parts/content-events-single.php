<?php
/**
* Template part for displaying page content in page.php
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/
*
* @package No_Dig
*/
?>
<?php
// get raw date
$date = get_field('date_of_events', false, false);
// make date object
$date = new DateTime($date);
?>
<!-- <p>Event start date: <?php //echo $date->format('F '); ?></p> -->
<?php
// increase by 1 day
$date->modify('+1 day');
  
?>
<!-- <p>Event end date: <?php //echo $date->format('j M Y'); ?></p> -->
<div class="container-fluid">
  <div class="row">
    <div class="col header_image" style="padding: 0px;">
      <?php
      $events_header_image = get_field('events_header_image', 'option');
      if (!empty($events_header_image)) { ?>
      <img src="<?php the_field('events_header_image', 'option');?>" alt="">
      <?php   }
      ?>
    </div>
  </div>
</div>
<div class="container">
  <div class="row outside">
    <div class="col-md-8 as-content-area"><!-- content holder and flexible content -->
    
    <div class="col-md-12 as-content-title">
      <h2><?php the_title();?></h2>
      <?php // get raw date
      $date = get_field('date_of_events', false, false);
      // make date object
      $date = new DateTime($date);
      ?>
      <p class="small"><?php echo $date->format('F d'); ?> @
        <?php
        $start_time = get_field('start_time_of_event');
        if (!empty($start_time)) {
        echo $start_time;
        }
        ?> - <?php
        $end_time = get_field('end_time_of_event');
        if (!empty($end_time)) {
        echo $end_time;
        }
        ?>&nbsp;&nbsp;<?php
        $cost = get_field('cost');
        if (!empty($cost)) {
        echo '£'.$cost;
        }
      ?></p>
      <?php
      $event_details = get_field('event_details');
      if (!empty($event_details)) {
      echo $event_details;
      }
      ?>
    </div>
    <?php
    $event_image = get_field('single_event_image');
    if ($event_image) { ?>
    
    <div class="col-md-12 as-single-image-section">
      <figure class="figure">
        <img src=" <?php the_field('single_event_image');?>" class="figure-img img-fluid rounded" alt="A generic square placeholder image with rounded corners in a figure.">
      </figure>
    </div>
    <?php  } ?>
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-12 as-front-testimonial"> <!-- Testimonial Block -->
        
        <div class="row as-slider-item h-100">
          <?php get_template_part( '/template-parts/content', 'testimonial-events'  );?>
        </div>
      </div>
      </div> <!-- /Testimonial Block -->
    </div>
    <div class="col-md-12 as-event-address">
      <div class="row">
        
        <div class="col-md-8">
          <div class="col-md-12">
            <h5 id="more_details" class="more_details">More Details</h5>
          </div>
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-2">
                Date:
              </div>
              <div class="col-md-8">
                <?php echo $date->format('d F Y'); ?>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-2">
                Time:
              </div>
              <div class="col-md-8">
                <?php
                $start_time = get_field('start_time_of_event');
                if (!empty($start_time)) {
                echo $start_time;
                }
                ?> - <?php
                $end_time = get_field('end_time_of_event');
                if (!empty($end_time)) {
                echo $end_time;
                }
                ?>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-2">
                Cost:
              </div>
              <div class="col-md-8">
                <?php
                $cost = get_field('cost');
                if (!empty($cost)) {
                echo '£'.$cost;
                }
                ?>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-2">
                Venue:
              </div>
              <div class="col-md-8">
                <?php the_field('event_address');?><br><?php the_field('event_address_two');?><br><?php the_field('event_address_three');?><br><?php the_field('event_address_four');?><br>
 
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-2">
                Phone:
              </div>
              <div class="col-md-8">
                <?php the_field('phone_number');?>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-2">
                Website:
              </div>
              <div class="col-md-8">
                <a href="<?php the_field('website');?>"><?php the_field('website');?> </a>
              </div>
            </div>
          </div>          
          
        </div>
        <div class="col-md-4">

            <?php 
             
            $location = get_field('event_map');
             
            if( !empty($location) ):
            ?>
            <div class="acf-map">
             <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
            </div>
            <?php endif; ?>

        </div>
      </div>
    </div>
  </div>
  <div class="col-md-4 stikey_sidebar as-single-sidebar text-center" id="sticky-sidebar"><!-- floating side bar -->
  <?php if ( has_post_thumbnail() ) : ?>
  <div class="row">
    <div class="col-md-12 as-thumbnail">
      
      
      <?php the_post_thumbnail(); ?>
      
      
    </div>
  </div>
  <?php endif; ?>
  
  <h2><?php the_title();?></h2>
  <?php // get raw date
  $date = get_field('date_of_events', false, false);
  $googledate = get_field('date_of_events', false, false);
  // make date object
  $date = new DateTime($date);
  ?>
  <p><?php echo $date->format('F d'); ?> @
    <?php
    $start_time = get_field('start_time_of_event');
    if (!empty($start_time)) {
    echo $start_time;
    }
    ?> - <?php
    $end_time = get_field('end_time_of_event');
    if (!empty($end_time)) {
    echo $end_time;
    }
    ?>&nbsp;&nbsp;<?php
    $cost = get_field('cost');
    if (!empty($cost)) {
    echo '£'.$cost;
    }
  ?></p>
  <p><?php the_field('event_address');?><br><?php the_field('event_address_two');?><br><?php the_field('event_address_three');?><br><?php the_field('event_address_four');?><br>
  <p>
  <a href="#more_details" class="btn btn-primary">FIND OUT MORE</a></p>
  <div class="row">
    <div class="col-md-12 as-add-to-cal">
      <a href="http://www.google.com/calendar/event?action=TEMPLATE&text=<?php the_title(); ?>&dates=<?php echo $googledate;?>/<?php echo $googledate;?>&details=<?php echo strip_tags( (get_field('event_details')));?>&location=<?php echo (get_field('event_address'));?>"target="_blank" rel="nofollow" class="btn btn-success">+ GOOGLE CALENDAR</a>
    </div>
  </div>

  <style>
    .as-add-to-cal {
      background-color: #45525c;
      padding: 20px;
      border-bottom-left-radius: 8px;
      border-bottom-right-radius: 8px;
    }

    .as-add-to-cal a.btn.btn-success {
      color:  #fff;
      background-color: #b6a269;
      border: 1px solid #b6a269;
      font-weight: 300;
    }

    .as-add-to-cal a.btn.btn-success:link {
      color:  #fff;
      background-color: #b6a269;
      border: 1px solid #b6a269;
      font-weight: 300;
    }

    .as-add-to-cal a.btn.btn-success:hover {
      color:  #fff;
      background-color: #b6a269;
      border: 1px solid #b6a269;
      font-weight: 300;
    }    
    .as-add-to-cal a.btn.btn-success:visited {
      color:  #fff;
      background-color: #b6a269;
      border: 1px solid #b6a269;
      font-weight: 300;
    } 

  </style>
  
</div>
</div>
</div>