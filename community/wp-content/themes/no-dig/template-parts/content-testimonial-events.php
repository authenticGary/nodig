<div id="testimonialFrontPage" class="carousel slide" data-ride="carousel">
	<ol class="carousel-indicators testimonial-indicators-numbers as-single-event">
		<?php
		$args = array (
			'post_type'              => array( 'testimonial' ),
			'post_status'            => array( 'publish' ),
			'nopaging'               => true,
			'order'                  => 'DESC',
			'orderby'                => 'date',
			'tax_query' => array(
		array(
		'taxonomy' => 'testimonial',
		'field' => 'slug', //can be set to ID
		'terms' => 'events' //if field is ID you can reference by cat/term number
		)
		)
		);
		// The Query
		$services = new WP_Query( $args );
		// The Loop
		if ( $services->have_posts() ) {
			$counter = 0;
			while ( $services->have_posts() ) {
				
		$services->the_post();?>
		<li data-target="#testimonialFrontPage" data-slide-to="<?php echo $counter;?>"></li>
		<?php $counter++;?>
		
		<?php }} else {
			// no posts found
		}
		// Restore original Post Data
		//wp_reset_postdata();
		?>
	</ol>
	<div class="carousel-inner">
		<?php
		$args = array (
		'post_type'              => array( 'testimonial' ),
		'post_status'            => array( 'publish' ),
		'nopaging'               => true,
		'order'                  => 'DESC',
		'orderby'                => 'date',
		'tax_query' => array(
		array(
		'taxonomy' => 'testimonial',
		'field' => 'slug', //can be set to ID
		'terms' => 'events' //if field is ID you can reference by cat/term number
		)
		)
		);
		// The Query
		$services = new WP_Query( $args );
		// The Loop
		if ( $services->have_posts() ) {
		while ( $services->have_posts() ) {
			
		$services->the_post();?>
		<div class="carousel-item carousel_user_image  mx-auto"">
			<?php $testimonial_img = get_field('user_image');?>
			<?php if (!empty($testimonial_img)) { ?>
				<div class="col-md-12 testimonial_image">
			<div class="quote_marks">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/svg/left.svg" class="svg_left">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/svg/right.svg" class="svg_right">
				</div>
				<img class="d-block" src="<?php the_field('user_image');?>">
			</div>
			<?php } else { ?>
			<?php /*?>	<div class="col-md-4 col-md-offset-4 as-single_testimonial_image text-center" style="max-width: 24%;">
				<div class="quote_marks_empty">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/svg/left.svg" class="svg_left">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/svg/right.svg" class="svg_right">
				</div>
				</div> <?php */ ?>
				<?php }?>
			
			
			<div class="text-center">
		    <p><?php the_field('user_testimonial')?></p>
		    <h5><?php the_field('user_name');?></h5>
		    <p><?php the_field('user_short_description')?></p>
		  </div>
		</div>
		<?php }} else {
			// no posts found
		}
		// Restore original Post Data
		wp_reset_postdata();
		?>
		
	</div>				
</div>