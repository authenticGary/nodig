<?php
/**
* Template part for displaying page content in page.php
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/
*
* @package No_Dig
*/
?>
<div class="container-fluid">
	<div class="row">
		<div class="col header_image" style="padding: 0px;">
			<?php
			$contact_header_image = get_field('contact_header_image');
			if (!empty($contact_header_image)) { ?>
			<img src="<?php the_field('contact_header_image');?>" alt="">
			<?php 	}
			?>
			
		</div>
	</div>
</div>
<div class="container">
	<div class="row outside">
		<div class="col-md-8 as-content-area as-contact-content"><!-- content holder and flexible content -->
		<!-- Title -->
		<div class="col-md-12 as-content-title">
			<h2><?php the_title();?></h2>
		</div>
		<!-- Content -->
		<div class="col-md-12 as-content-section">
			<?php the_content();?>
		</div>

		<!-- address Section -->

   <div class="col-md-12 as-contact-address">
      <div class="row">
        
        <div class="col-md-8">
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-2 as-contact-labels">
                Location:
              </div>
              <div class="col-md-8">
                <?php the_field('location'); ?>
              </div>
            </div>
          </div>
          <div class="col-md-12 as-contact-row">
            <div class="row">
              <div class="col-md-2 as-contact-labels">
                Phone:
              </div>
              <div class="col-md-8">
                <?php the_field('phone_number');?>
              </div>
            </div>
          </div>
          <div class="col-md-12 as-contact-row">
            <div class="row">
              <div class="col-md-2 as-contact-labels">
                Email:
              </div>
              <div class="col-md-8">
                <?php the_field('email_address');?>
              </div>
            </div>
          </div>
          <div class="col-md-12 as-contact-row">
            <div class="row">
              <div class="col-md-2 as-contact-labels">
                Social:
              </div>
              <div class="col-md-8">
					<?php if ( is_active_sidebar( 'social_icons' ) ) : ?>
						<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
							<?php dynamic_sidebar( 'social_icons' ); ?>
						</div><!-- #primary-sidebar -->
					<?php endif; ?>
              </div>
            </div>
          </div>         
          
        </div>
        <div class="col-md-4">

            <?php 
             
            $location = get_field('contact_map');
             
            if( !empty($location) ):
            ?>
            <div class="acf-map">
             <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
            </div>
            <?php endif; ?>

        </div>
      </div>
    </div>

    <!-- /Address Section -->

		
	</div>
	<div class="col-md-4 stikey_sidebar as-contact-page" id="sticky-sidebar"><!-- floating side bar -->
		<h2>Send us a message</h2>

		<?php echo do_shortcode('[contact-form-7 id="49916" title="Contact Page"]');?>
	
</div>
</div>
</div>