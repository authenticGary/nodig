<a href="<?php the_permalink();?>">
<div class="col-md-12 blog-sing-row">
	<div class="row as-row">
		<?php $featured_img_url = get_the_post_thumbnail_url($post->ID, 'full');  ?>
		<div class="col-md-4 post-list-image" style="background-image: url(<?php echo $featured_img_url;?>);">
			<?php /* if ( has_post_thumbnail() ) { the_post_thumbnail(array( 200, 200, true ));} */ ?>
		</div>
		<div class="col-md-8 post-list-excerpt">
			<h2><?php the_title();?></h2>
			<p><?php echo substr(get_the_excerpt(), 0,200); ?></p>
			
		</div>
	</div>
</div>
</a>