	<div class="col-md-12 as-single_testimonial text-center">		
			<div class="row">
				
				<?php if(get_sub_field('single_testimonial_image')) { ?>
					<div class="col-md-12 as-single_testimonial_image">
				<div class="quote_marks">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/svg/left.svg" class="svg_left">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/svg/right.svg" class="svg_right">
				</div>
					<img src="<?php the_sub_field('single_testimonial_image');?>">
				</div>
				<?php } else { ?>
					<div class="col-md-12 as-single_testimonial_image" style="max-width: 24%;">
				<div class="quote_marks_empty">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/svg/left.svg" class="svg_left">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/svg/right.svg" class="svg_right">
				</div>
				</div>
				<?php }?>
					

				
				<div class="col-md-12 as-single_testimonial_text">
					<?php the_sub_field('single_testimonial_text');?>
					
				</div>
				<div class="col-md-12 as-single_testimonial_name">
					<?php the_sub_field('single_testimonial_name');?>
					
				</div>
				<div class="col-md-12 as-single_testimonial_role">
					<?php the_sub_field('single_testimonial_role');?>
					
				</div>
			</div>
		</div>