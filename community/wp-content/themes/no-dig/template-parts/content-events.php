<?php
/**
* Template part for displaying page content in page.php
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/
*
* @package No_Dig
*/
?>
<div class="col-md-12 as-events-archive-date">
  <div class="row">
    <?php $featured_img_url = get_the_post_thumbnail_url($post->ID, 'full');  ?>
    <div class="col-md-3 post-list-image as-event-image" style="background-image: url(<?php echo $featured_img_url;?>); min-height: 185px; max-height: 185px;">
      
    </div>
    <div class="col-md-9 as-archive-event-details">
      <h4><?php the_title();?></h4>


      <?php // get raw date
      $date = get_field('date_of_events', false, false);
      // make date object
      $date = new DateTime($date);
      ?>
      <p class="small"><i class="far fa-calendar"></i> &nbsp;<?php echo $date->format('F d'); ?> @
        <?php
        $start_time = get_field('start_time_of_event');
        if (!empty($start_time)) {
        echo $start_time;
        }
        ?> - <?php
        $end_time = get_field('end_time_of_event');
        if (!empty($end_time)) {
        echo $end_time;
        }
        ?>&nbsp;
      <i class="fas fa-map-marker-alt"></i>&nbsp;<?php the_field('event_address');?>&nbsp;<?php the_field('event_address_three');?>
      </p>

      <?php echo custom_field_excerpt(); ?>

      <a class="btn btn-primary"href="<?php the_permalink();?>">Find out more</a>
    </div>
  </div>
</div>

        

