<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package No_Dig
 */

?>
<div class="card">
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php no_dig_post_thumbnail(); ?>

	<div class="card-body">

			<div class="entry-meta">
			<?php
			no_dig_posted_on();
			no_dig_posted_by();
			?>
		</div><!-- .entry-meta -->

		
	<header class="entry-header">
		<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a>here</h2>' ); ?>

		<?php if ( 'post' === get_post_type() ) : ?>

		<?php endif; ?>
	</header><!-- .entry-header -->

	

	<div class="entry-summary">
		<a href="<?php the_permalink();?>"><?php the_excerpt(); ?></a>
	</div><!-- .entry-summary -->

	<footer class="entry-footer">
		<?php no_dig_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</div>
</article><!-- #post-<?php the_ID(); ?> -->
</div>

