<?php
/**
* Template part for displaying page content in page.php
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/
*
* @package No_Dig
*/
?>
<div class="container-fluid">
	<div class="row">
		<div class="col header_image" style="padding: 0px;">
			<?php
				$header_image = get_field('header_image');
			if (!empty($header_image)) { ?>
			<img src="<?php the_field('header_image');?>" alt="">
			<?php 	}
			?>
			
		</div>
	</div>
</div>
<div class="container">
	<div class="row outside">
		<div class="col-md-8 as-content-area"><!-- content holder and flexible content -->
		<?php
			// check if the flexible content field has rows of data
			if( have_rows('blocks') ):
			// loop through the rows of data
			while ( have_rows('blocks') ) : the_row();
		if( get_row_layout() == 'section_title' ): ?>
		<div class="col-md-12 as-content-title">
			<?php $string = get_sub_field('title');
            $result = preg_replace('/[^a-zA-Z0-9-_\.]/','', $string);?>
			<h2 id="<?php echo $result?>"><?php the_sub_field('title');?></h2>
			<p class="small"><?php the_sub_field('sub_title');?></p>
			<a href="#" name="<?php echo get_row_layout() == 'section_title' ;?>"></a>
		</div>
		
		<?php elseif( get_row_layout() == 'single_column_text' ): ?>
		<div class="col-md-12 as-content-section">
			<?php the_sub_field('single_column');?>
		</div>
		<?php elseif( get_row_layout() == 'single_column_image' ): ?>
		<div class="col-md-12 as-single-image-section">
			<figure class="figure">
				<img src=" <?php the_sub_field('single_image');?>" class="figure-img img-fluid rounded" alt="A generic square placeholder image with rounded corners in a figure.">
				<figcaption class="figure-caption text-center"><?php the_sub_field('image_caption');?></figcaption>
			</figure>
		</div>
		<?php elseif( get_row_layout() == 'single_column_summary' ): ?>
		<div class="col-md-12 summary">
			<div class="row">
				<div class="col-md-1 summary-icon">
					<!-- i class="fas fa-leaf"></i -->
					<i class="<?php the_sub_field('summary_icon');?>"></i>
				</div>
				<div class="col-md-11 summary-text">
					<?php the_sub_field('summary_text');?>
					
				</div>
			</div>
		</div>
		<?php elseif( get_row_layout() == 'single_testimonial' ): ?>
		<div class="col-md-12 as-single_testimonial text-center">
			<div class="row">
				
				<?php if(get_sub_field('single_testimonial_image')) { ?>
					<div class="col-md-12 as-single_testimonial_image">
				<div class="quote_marks">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/svg/left.svg" class="svg_left">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/svg/right.svg" class="svg_right">
				</div>
					<img src="<?php the_sub_field('single_testimonial_image');?>">
				</div>
				<?php } else { ?>
					<div class="col-md-12 as-single_testimonial_image" style="max-width: 24%;">
				<div class="quote_marks_empty">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/svg/left.svg" class="svg_left">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/svg/right.svg" class="svg_right">
				</div>
				</div>
				<?php }?>
					

				
				<div class="col-md-12 as-single_testimonial_text">
					<?php the_sub_field('single_testimonial_text');?>
					
				</div>
				<div class="col-md-12 as-single_testimonial_name">
					<?php the_sub_field('single_testimonial_name');?>
					
				</div>
				<div class="col-md-12 as-single_testimonial_role">
					<?php the_sub_field('single_testimonial_role');?>
					
				</div>
			</div>
		</div>
		<?php elseif( get_row_layout() == 'cta' ): ?>
		<div class="col-md-12 cta">
			<div class="row h-100">
				<div class="col-md-3 my-auto the-icon">
					<!-- i class="fas fa-users"></i -->
					<i class="<?php the_sub_field('cta_icon');?>"></i>
				</div>
				<div class="col-md-8">
					<h4><?php the_sub_field('cta_title');?></h4>
					<?php the_sub_field('cta_text');?>
					
				</div>
				<div class="col-md-1 my-auto">
					<a href="<?php the_sub_field('cta_link')?>"><span class="align-middle"><i class="fas fa-angle-right"></i></span></a>
				</div>
			</div>
		</div>
		<?php elseif( get_row_layout() == 'two_column_image' ): ?>
		<div class="row as-two-image-container">
			<div class="col-md-6 as-twocol-image-section-left">
				<figure class="figure">
					<img src=" <?php the_sub_field('two_column_image_left');?>" class="figure-img img-fluid rounded" alt="A generic square placeholder image with rounded corners in a figure.">
					<figcaption class="figure-caption text-center"><?php the_sub_field('two_column_image_left_caption');?></figcaption>
				</figure>
			</div>
			<div class="col-md-6 as-twocol-image-section-right">
				<figure class="figure">
					<img src=" <?php the_sub_field('two_column_image_right');?>" class="figure-img img-fluid rounded" alt="A generic square placeholder image with rounded corners in a figure.">
					<figcaption class="figure-caption text-center"><?php the_sub_field('two_column_image_right_caption');?></figcaption>
				</figure>
			</div>
		</div>
		<?php elseif( get_row_layout() == 'product' ): ?><!-- Single Product -->
		<div class="col-md-12 content_page_woocommerce">
			<?php
			$product = get_sub_field('single_product');
		    //print_r($product);
			//echo do_shortcode('[product_page id="'.$product.'"]');
			?>
			
			
			<?php
			$args = array(
			'post_type' => 'product',
			'p' => $product,
			'posts_per_page' => 1,
			
			);
			$loop = new WP_Query( $args );
			if ( $loop->have_posts() ) {
			while ( $loop->have_posts() ) : $loop->the_post();
			get_template_part( '/woocommerce/templates/content', 'productcontent'  );
			endwhile;
			} else {
			echo __( 'No products found' );
			}
			wp_reset_postdata();
			?>
		
		</div>
		<?php elseif( get_row_layout() == 'single_quote' ): ?>
		<div class="col-md-12 single_quote">
			<?php the_sub_field('single_quote_text');?>
		</div>
		<?php endif;
		endwhile;
		else :
		// no layouts found
		endif;
		?>
		
	</div>
	<div class="col-md-4 stikey_sidebar as-content-sidebar" id="sticky-sidebar"><!-- floating side bar -->
		<h2 class="sidebar-title">Contents</h2>
	<?php
			// check if the flexible content field has rows of data
			if( have_rows('blocks') ):
			// loop through the rows of data
			while ( have_rows('blocks') ) : the_row();
	if( get_row_layout() == 'section_title' ): 
		$string = get_sub_field('title');
        $result = preg_replace('/[^a-zA-Z0-9-_\.]/','', $string);?>
	<p><spna class="in-page-icon"><i class="fal fa-leaf"></i></spna> &nbsp; <a class="as-inpage-link" href="#<?php echo $result;?>"><?php the_sub_field('title');?></a></p>
	<?php elseif( get_row_layout() == 'section_title' ): ?>
	<?php endif;
	endwhile;
	else :
	// no layouts found
	endif;
	?>
</div>
</div>
</div>