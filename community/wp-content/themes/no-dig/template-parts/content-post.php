<?php
/**
* Template part for displaying page content in page.php
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/
*
* @package No_Dig
*/
?>

    <div class="card mr-3 mb-3 as-card">
               <?php if ( has_post_thumbnail() ) {
               the_post_thumbnail('full');
         } else { ?>
     <img src="<?php bloginfo('template_directory'); ?>/assets/images/default-image.jpg" alt="<?php the_title(); ?>" style="border-top-left-radius: 8px; border-bottom-left-radius: 8px;"/>
      <?php } ?>
      <div class="card-body">
        <h5 class="card-title"><?php the_title();?></h5>
        <p class="card-text"><?php echo substr(get_the_excerpt(), 0,200); ?></p>
        <a href="<?php the_permalink();?>" class="btn btn-primary">READ MORE</a>
      </div>
    </div>


	
				

