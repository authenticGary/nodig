<div class="container as-site-info as-footer-mobile">
	<div class="row as-widget-areas">
		<div class="col-md-12 text-center" style="margin-bottom: 30px;"><!-- Widget 1 -->
		<?php if ( is_active_sidebar( 'footer_one' ) ) : ?>
		<div id="footer_one" class="primary-sidebar widget-area" role="complementary">
			<?php dynamic_sidebar( 'footer_one' ); ?>
			</div><!-- #primary-sidebar -->
			<?php endif; ?>
		</div>
		<div class="accordion" id="accordionExample" syle="margin: 20px 0px;">
			<div class="card text-center">
				<div class="card-header" id="headingOne">
					<h2 class="mb-0 text-center">
					<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
					SHOP
					</button>
					</h2>
				</div>
				<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
					<div class="card-body">
						<div class="col-md-12 d-md-flex flex-md-row-reverse"><!-- Widget 2 -->
						<?php if ( is_active_sidebar( 'footer_two' ) ) : ?>
						<div id="footer_two" class="primary-sidebar widget-area" role="complementary">
							<?php dynamic_sidebar( 'footer_two' ); ?>
							</div><!-- #primary-sidebar -->
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
			<div class="card text-center">
				<div class="card-header" id="headingTwo">
					<h2 class="mb-0">
					<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
					LEARN
					</button>
					</h2>
				</div>
				<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
					<div class="card-body">
						<div class="col-md-12 d-md-flex flex-md-row-reverse"><!-- Widget 3 -->
						<?php if ( is_active_sidebar( 'footer_three' ) ) : ?>
						<div id="footer_threer" class="primary-sidebar widget-area" role="complementary">
							<?php dynamic_sidebar( 'footer_three' ); ?>
							</div><!-- #primary-sidebar -->
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
			<div class="card text-center">
				<div class="card-header" id="headingThree">
					<h2 class="mb-0">
					<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
					QUESTIONS?
					</button>
					</h2>
				</div>
				<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
					<div class="card-body">
						<div class="col-md-12 d-md-flex flex-md-row-reverse"><!-- Widget 4 -->
						<?php if ( is_active_sidebar( 'footer_four' ) ) : ?>
						<div id="footer_fourr" class="primary-sidebar widget-area" role="complementary">
							<?php dynamic_sidebar( 'footer_four' ); ?>
							</div><!-- #primary-sidebar -->
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
			<div class="card text-center">
				<div class="card-header" id="headingFour">
					<h2 class="mb-0">
					<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
					COMPANY
					</button>
					</h2>
				</div>
				<div id="collapseFour" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
					<div class="card-body">
						<div class="col-md-12 d-md-flex flex-md-row-reverse"><!-- Widget 5 -->
						<?php if ( is_active_sidebar( 'footer_five' ) ) : ?>
						<div id="footer_five" class="primary-sidebar widget-area" role="complementary">
							<?php dynamic_sidebar( 'footer_five' ); ?>
							</div><!-- #primary-sidebar -->
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-12 text-center" style="margin-top: 30px;"><!-- Widget 6 -->
		<?php if ( is_active_sidebar( 'footer_six' ) ) : ?>
		<div id="footer_sixr" class="primary-sidebar widget-area" role="complementary">
			<?php dynamic_sidebar( 'footer_six' ); ?>
			</div><!-- #primary-sidebar -->
			<?php endif; ?>
		</div>
	</div>
	<div class="row text-center">
		
		
		<div class="col-md-12 justify-content-center align-self-center text-center">
			<p>&copy; Copyright Charles Dowding <?php echo date("Y"); ?></p>
		</div>
		<div class="col-md-12 text-center">
			<p><a href="https://authenticstyle.co.uk/" style="color: #404040">Web design Dorset</a> by <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/shape.png">&nbsp;authentic <b>style</b></p>
			</div>
			
			
		</div>
	</div>