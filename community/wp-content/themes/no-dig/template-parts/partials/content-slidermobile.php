<div data-ride="carousel" class="carousel slide carousel-fade" id="carouselExampleIndicators">
	<div role="listbox" class="carousel-inner">
		<!-- Indicators -->
		<ol class="carousel-indicators carousel-indicators-numbers">
			<?php if( have_rows('slider') ): $counter = 0 ?>
			<?php while( have_rows('slider') ): the_row();
			?>
			<li data-target="#carouselExampleIndicators	" data-slide-to="<?php echo $counter;?>"><?php echo get_row_index(); ?></li>
			<?php $counter++; endwhile; ?>
			
			<?php endif; ?>
		</ol>
		
		<?php if( have_rows('slider') ): ?>
		<?php while( have_rows('slider') ): the_row();
			
			// vars
			$image = get_sub_field('slider_image');
			$content = get_sub_field('slider_text_block');
		?>
		
		<?php $slider = $image['url']; ?>
		<div class="carousel-item header_slider" style="background-image: url(<?php echo $slider;?>); position: center center; height: 400px; background-repeat: no-repeat;">
			
			
			<div class="carousel-caption">
				<?php
				$post_object = get_sub_field('post');
				if( $post_object ):
				// override $post
				$post = $post_object;
				setup_postdata( $post );
				?>
				<div>
					<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
					<p><i class="far fa-clock"></i> <?php
						echo the_time('jS F  Y'); ?>&nbsp; &nbsp;
						<i class="fas fa-tag"></i> <?php $categories = get_the_category();
						
							if ( ! empty( $categories ) ) {
							echo esc_html( $categories[0]->name );
							}
					?></p>
				</div>
				<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
				<?php endif; ?>
			</div>
		</div>
		
		<?php endwhile; ?>
		
		<?php endif; ?>
		
		<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>
	
</div>