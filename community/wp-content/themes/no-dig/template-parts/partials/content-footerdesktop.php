<div class="container as-site-info">
	<div class="row as-widget-areas">
		<div class="col-md-1"><!-- Widget 1 -->
			<?php if ( is_active_sidebar( 'footer_one' ) ) : ?>
				<div id="footer_one" class="primary-sidebar widget-area" role="complementary">
					<?php dynamic_sidebar( 'footer_one' ); ?>
				</div><!-- #primary-sidebar -->
			<?php endif; ?>
		</div>

		<div class="col-md-2 d-md-flex flex-md-row-reverse"><!-- Widget 2 -->
			<?php if ( is_active_sidebar( 'footer_two' ) ) : ?>
				<div id="footer_two" class="primary-sidebar widget-area" role="complementary">
					<?php dynamic_sidebar( 'footer_two' ); ?>
				</div><!-- #primary-sidebar -->
			<?php endif; ?>
		</div>

		<div class="col-md-2 d-md-flex flex-md-row-reverse"><!-- Widget 3 -->
			<?php if ( is_active_sidebar( 'footer_three' ) ) : ?>
				<div id="footer_threer" class="primary-sidebar widget-area" role="complementary">
					<?php dynamic_sidebar( 'footer_three' ); ?>
				</div><!-- #primary-sidebar -->
			<?php endif; ?>
		</div>

		<div class="col-md-2 d-md-flex flex-md-row-reverse"><!-- Widget 4 -->
			<?php if ( is_active_sidebar( 'footer_four' ) ) : ?>
				<div id="footer_fourr" class="primary-sidebar widget-area" role="complementary">
					<?php dynamic_sidebar( 'footer_four' ); ?>
				</div><!-- #primary-sidebar -->
			<?php endif; ?>
		</div>

		<div class="col-md-2 d-md-flex flex-md-row-reverse"><!-- Widget 5 -->
			<?php if ( is_active_sidebar( 'footer_five' ) ) : ?>
				<div id="footer_five" class="primary-sidebar widget-area" role="complementary">
					<?php dynamic_sidebar( 'footer_five' ); ?>
				</div><!-- #primary-sidebar -->
			<?php endif; ?>
		</div>

		<div class="col-md-3 d-md-flex flex-md-row-reverse"><!-- Widget 6 -->
			<?php if ( is_active_sidebar( 'footer_six' ) ) : ?>
				<div id="footer_sixr" class="primary-sidebar widget-area" role="complementary">
					<?php dynamic_sidebar( 'footer_six' ); ?>
				</div><!-- #primary-sidebar -->
			<?php endif; ?>
		</div>

	</div>
	<div class="row d-md-flex h-100">
	
		
			<div class="col-md-6 text-md-left justify-content-center align-self-center">
							<p>&copy; Copyright Charles Dowding <?php echo date("Y"); ?></p>
			</div>
			<div class="col-md-6 text-md-right justify-content-center align-self-center">
				<p><a href="https://authenticstyle.co.uk/" style="color: #404040">Web design Dorset</a> <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/shape.png">&nbsp;authentic <b>style</b></p>
			</div>
		
	

	</div>
</div>