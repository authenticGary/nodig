<?php
/**
* Template part for displaying posts
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/
*
* @package No_Dig
*/
?>
<style>
	
.as-forum-login,
.as-forum-register {
	margin-top: 40px;
	margin-bottom: 40pc;
}
.as-forum-login {
	background: #fff;
    border-radius: 8px;
    padding: 40px;
}

.as-forum-login label {
	width:  100%;
}

input#user_login,
input#user_pass{
	width: 100% !important;
    height: 40px !important;
    background-color: #f7f8fa !important;
}

input#wp-submit {
	color:  #fff !important;
	background-color: #66883f !important;
    border-color: #66883f !important;
    font-weight: 200 !important;
    padding: .475rem 1.2rem !important;
	display: inline-block !important;
    font-weight: 400 !important;
    text-align: center !important;
    white-space: nowrap !important;
    vertical-align: middle !important;
    user-select: none !important;
    border: 1px solid transparent !important;
    padding: .375rem .75rem !important;
    font-size: 1rem !important;
    line-height: 1.5 !important;
    border-radius: .25rem !important;
    transition: color 0.15s ease-in-out,background-color 0.15s ease-in-out,border-color 0.15s ease-in-out,box-shadow 0.15s ease-in-out;
}

</style>
<div class="row justify-content-center">
	
	<div class="col-6 align-self-center as-forum-login">
		<div class="col-md-12"><!-- Widget 6 -->
		<?php if ( is_active_sidebar( 'login_page' ) ) : ?>
		<div id="login_page" class="primary-sidebar widget-area" role="complementary">
			<?php dynamic_sidebar( 'login_page' ); ?>
			</div><!-- #primary-sidebar -->
			<?php endif; ?>
		</div>
	</div>
</div>