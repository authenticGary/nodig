<?php
/**
* Template part for displaying page content in page.php
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/
*
* @package No_Dig
*/
?>
<div class="container-fluid">
	<div class="row">

		 <?php 

             $detect = new Mobile_Detect;
             $deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');
             $scriptVersion = $detect->getScriptVersion();

			 if ( $detect->isMobile() && !$detect->isTablet() ) {

		 get_template_part( '/template-parts/partials/content', 'slidermobile'  );

		} else {
             	 get_template_part( '/template-parts/partials/content', 'sliderdesktop'  );
             }
			 ?>

	</div>
	<div class="container h-100 chalers_intro">
		<div class="row align-items-center h-100">
			<div class="col-md-6 as-charles">
				<?php
				$intro_image = get_field('intro_image');
				if (!empty($intro_image)) { ?>
				<img src="<?php echo $intro_image; ?>">
				<?php	} ?>
				
			</div>
			<div class="col-md-6 intro_copy">
				<div class="col-md-12 text-center mx-auto">
					<h3 class="text-center"><?php the_field('intro_title'); ?></h3>
					<?php the_field('intro_copy');?>
					<a href="<?php the_field('intro_button_link');?>" title="" class="btn btn-primary">START HERE</a>
				</div>
				
				
			</div>
			
		</div>
	</div>
	<!-- Owl Carousel Repeater -->
	<?php if( have_rows('logos') ): ?>
	<div class="container owl_carousel">
		<div class="row">
			<div class="col-md-12 carousel_title">
				<h3 class="text-center">As Seen In</h3>
			</div>
			<div class="col-md-12">
				
				<div class="owl-carousel">
					<?php while( have_rows('logos') ): the_row();  ?>
					<div><img src="<?php the_sub_field('logo');?>" style="display: inline-block; vertical-align: middle !important;"></div>
					<?php endwhile; ?>
				</div>
			</div>
		</div>
	</div>
	<?php endif; ?>
	<!-- Owl Carousel -->
	<!-- Benefits of No Dig -->
	<div class="container as_benefits">
		<div class="row">
			<div class="col-md-12 benefit_block text-center">
				<div class="row">
					
					
					<div class="col-md-3">
						<span  class="benefits_title"><?php the_field('benefit_title')?>: &nbsp;</span>
					</div>
					<div class="col-md-3 text-white as_reasons">
						<i class="fas fa-check-circle"></i>&nbsp;<?php the_field('benefit_one')?>
					</div>
					<div class="col-md-3 text-white as_reasons">
						<i class="fas fa-check-circle"></i>&nbsp;<?php the_field('benefit_two')?>
					</div>
					<div class="col-md-3 text-white as_reasons">
						<i class="fas fa-check-circle"></i>&nbsp;<?php the_field('benefit_three')?>
					</div>
				</div>
				
				
			</div>
			
		</div>
	</div>
	<!-- Benefits of No Dig  -->
</div>
<!-- Start of  -->
<div class="container-fluid as-brown-bg">
	<div class="row"><!-- Video Section -->
	<div class="container as-video">
		<div class="col-md-12">
			<?php
				$video_section = get_field('video_link');
			if (!empty($video_section)) { ?>
			
			
			<div class="embed-container">
				<?php
									// get iframe HTML
									$iframe = get_field('video_link');
									// use preg_match to find iframe src
									preg_match('/src="(.+?)"/', $iframe, $matches);
									$src = $matches[1];
									// add extra params to iframe src
									$params = array(
									'controls'    => 1,
									'hd'        => 1,
									'autohide'    => 1
									);
									$new_src = add_query_arg($params, $src);
									$iframe = str_replace($src, $new_src, $iframe);
									// add extra attributes to iframe html
									$attributes = 'frameborder="0"';
									$iframe = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $iframe);
									// echo $iframe
									echo $iframe;
				?>
				<style>
					.embed-container {
						position: relative;
						padding-bottom: 56.25%;
						overflow: hidden;
						max-width: 100%;
						height: auto;
						border-radius: 10px;
					}
					.embed-container iframe,
					.embed-container object,
					.embed-container embed {
						position: absolute;
						top: 0;
						left: 0;
						width: 100%;
						height: 100%;
					}
				</style>
			</div>
			<?php 	} ?>
			
		</div>
		
	</div>
	
	</div><!--/ Video Section -->
	<div class="container as-features"><!-- product Section and testimonial Section -->
	<div class="row">
		<div class="col-md-6 as-course-product"> <!-- product block -->
		<div class="col-md-12">
			
			
			<?php
			$args = array(
			'post_type' => 'product',
			'posts_per_page' => 2,
			'tax_query' => array(
			array(
			'taxonomy' => 'product_visibility',
			'field'    => 'name',
			'terms'    => 'featured',
			),
			),
			);
			$loop = new WP_Query( $args );
			if ( $loop->have_posts() ) {
			while ( $loop->have_posts() ) : $loop->the_post();
			get_template_part( '/woocommerce/templates/content', 'producthome'  );
			endwhile;
			} else {
			echo __( 'No products found' );
			}
			wp_reset_postdata();
			?>
		</div>
		</div><!-- product block -->
		<div class="col-md-6 as-front-testimonial"> <!-- Testimonial Block -->
		<div class="col-md-12">
			<div class="row as-slider-item front_page_testimonial">
				<?php get_template_part( '/template-parts/content', 'testimonial'  );?>
			</div>
			
			
			
		</div>
		
		</div> <!-- /Testimonial Block -->
	</div>
	
</div>
<div class="container as-instagram"> <!-- Instgram Block -->
<div class="row">
	
	<div class="col-md-12 as-instgram-feed">
		<?php echo do_shortcode('[wdi_feed id="1"]');?>
	</div>
	
</div>

</div> <!-- /Instgram Block -->
<div class="container as-forum-link"> <!-- CTA Block -->
<div class="row">
	
	<div class="col-md-12 cta">
		<div class="row h-100">
			<div class="col-md-2 my-auto the-icon">
				<!-- i class="fas fa-users"></i -->
				<i class="<?php the_field('cta_icon');?>"></i>
			</div>
			<div class="col-md-9">
				<h4><?php the_field('cta_title');?></h4>
				<?php the_field('cta_text');?>
				
			</div>
			<div class="col-md-1 my-auto as-community-link">
				<a href="<?php the_field('community_page_link');?>"><span class="align-middle"><i class="fas fa-angle-right"></i></span></a>
			</div>
		</div>
	</div>
	
	
</div>

</div> <!-- /CTA Block -->
<div class="container">
	<div class="row">
		<div class="col-md-12 as-four-product"> <!-- product block -->
		<div class="row home_products">
			
			
			<?php
			$args = array(
			'post_type' => 'product',
			'posts_per_page' => 4,
			'orderby'   => 'date',
			'order' => 'ASC',
			'tax_query' => array(
			array(
			'taxonomy' => 'product_cat',
			'field'    => 'slug',
			'terms'    => 'front-four',
			),
			),
			);
			$loop = new WP_Query( $args );
			if ( $loop->have_posts() ) {
			while ( $loop->have_posts() ) : $loop->the_post();
			get_template_part( '/woocommerce/templates/content', 'producthome-alt'  );
			endwhile;
			} else {
			echo __( 'No products found' );
			}
			wp_reset_postdata();
			?>
		</div>
		
	</div>
</div>
</div>
</div>
<div class="container-fluid blog-bg"><!-- Blog based on Season-->
	<div class="container">
		<div class="row">
		<div class="col-md-6"><!-- Main Season Indicator -->


			<?php get_template_part( '/template-parts/content', 'season-left'  );?>


		</div>	
		<div class="col-md-6 seasoned-posts"><!-- Blog section -->
			<?php $season = get_field('season'); 

                    switch ($season ) {
                    	case 'Summer':
                    		$seasons_display = 'summer';
                    		break;

                    	case 'Winter':
                    		$seasons_display = 'winter';
                    		break;	
                    	
                    	case 'Autumn':
                    		$seasons_display = 'autumn';
                    		break;

                       	case 'Spring':
                    		$seasons_display = 'spring';
                    		break;
                    			
                    	default:
                    		$seasons_display = 'summer';
                    		break;
                    }
            ?>
			<?php
			$args = array(
			'post_type' => 'post',
			'posts_per_page' => 4,
			'tax_query' => array(
			array(
			'taxonomy' => 'category',
			'field'    => 'slug',
			'terms'    => $seasons_display,
			),
			),
			);
			$loop_blog = new WP_Query( $args );
			if ( $loop_blog->have_posts() ) {
			while ( $loop_blog->have_posts() ) : $loop_blog->the_post();
			get_template_part( '/template-parts/content', 'season'  );
			endwhile;
			} else {
			echo __( 'No posts were found' );
			}
			wp_reset_postdata();
			?>
		</div>	
	</div>
</div>
</div>
