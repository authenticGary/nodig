<header id="masthead" class="site-header">
	<div class="container">
		<div class="row">
			
			<div class="col-md-2 pull-left header_left header-icons">
				<a href="<?php the_field('twitter', 'option');?>" target="_blank"><i class="fab fa-twitter"></i></a>&nbsp;
				<a href="<?php the_field('youtube', 'option');?>" target="_blank"><i class="fab fa-youtube"></i></a>&nbsp;
				<a href="<?php the_field('facebook', 'option');?>" target="_blank"><i class="fa fa-facebook-f"></i></a>&nbsp;
				<a href="<?php the_field('instagram', 'option');?>" target="_blank"><i class="fab fa-instagram"></i></a>
			</div>
			<div class="col-md-8">
				<div class="site-branding">
					<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><i class="fas fa-bars"></i></button>
					<?php
					the_custom_logo();?>
					</div><!-- .site-branding -->
				</div>
				<div class="col-md-2 header_right pull-right text-right header-icons">
					
					<a href="javascrip:void(0)" data-toggle="modal" data-target="#exampleModalCenter"><i class="far fa-search"></i>&nbsp;</a>
					<a href="<?php the_field('user_login', 'option');?>"><i class="far fa-user"></i>&nbsp;
					<a href="<?php echo wc_get_cart_url(); ?>"><i class="far fa-shopping-cart"></i><span class="basket-items-count"><?php echo sprintf ( _n( '%d', '%d', WC()->cart->get_cart_contents_count() ), WC()->cart->get_cart_contents_count() ); ?></span></a>		

				</div>
				</div><!-- #Row -->
				<div class="row header_bottom"> <!-- Menu row -->
				
				<div class="col-sm charles_stor header_left">
					<a href="<?php the_field('charles_story', 'option');?>">Charles' Story</a>
				</div>
				<div class="col-md-8">
					
					<nav id="site-navigation" class="main-navigation">
						
						<?php
						wp_nav_menu( array(
							'theme_location' => 'menu-1',
							'menu_id'        => 'primary-menu',
							'menu_class'    => 'list-inline mx-auto justify-content-center',
						) );
						?>
						</nav><!-- #site-navigation -->
					</div>
					<div class="col-md-2 header_right header_right_button text-right">
						<a href="<?php the_field('start_here_button', 'option');?>" class="btn btn-info">START HERE</a>
					</div>
					</div><!-- #Row -->
					</div><!-- #Container -->
					</header><!-- #masthead -->