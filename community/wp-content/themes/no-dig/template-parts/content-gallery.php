<?php
/**
* Template part for displaying page content in page.php
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/
*
* @package No_Dig
*/
?>
<div class="container-fluid">
  <div class="row">
    <div class="col header_image" style="padding: 0px;">
      <?php
      $default_header_image = get_field('default_header_image', 'option');
      if (!empty($default_header_image)) { ?>
      <img src="<?php the_field('default_header_image', 'option');?>" alt="">
      <?php   }
      ?>
      
    </div>
  </div>
</div>
<div class="container">
	<div class="row outside">

		<!-- The Title -->
		<div class="col-md-12 as-content-title">
			<h2><?php the_title();?></h2>
		</div>

		<!-- Content Area -->
        <div class="col-md-12 as-content-section as-gallery-content">
			<?php the_content();?>
		</div>

				<!-- The Gallery -->
        <div class="col-md-12 as-content-section as-gallery-section">
        	<div class="row">
        		<div class="card-columns">
        		<?php echo do_shortcode('[ajax_load_more preloaded="true" preloaded_amount="12" posts_per_page="12" pause="false" pause_override="true" scroll="true" button_label="Load More" button_loading_label="Loading..." acf="true" acf_field_type="gallery" acf_field_name="image_gallery" seo="true" container_type="div" transition="fade" images_loaded="true"]');?>
        		</div>
        	</div>
		</div>
</div>
</div>