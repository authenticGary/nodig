<?php
/**
* Template Name: Learn Archive
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/
*
* @package No_Dig
*/
get_header();
?>
<div class="container-fluid as-leader-header">
	<div class="container">
		<div class="row">
			<div class="col-md-12 as-category-slider">
				
				<div>
					<?php
							$args = array(
							'orderby' => 'name',
							'order' => 'ASC'
							);
					$categories = get_categories($args); ?>
					<div class="owl-carousel gray-shadow">
						<?php foreach($categories as $category) {
						// check if image field is set on category
						if ( get_field( 'category_image', 'category_' . $category->term_id ) ) {
						// display link
						echo '<a class="cover margin-ultimos" href="' . get_category_link( $category->term_id ) . '"><img src="'. get_field( 'category_image', 'category_'.$category->term_id ). '" alt="Portada'. get_cat_name ( $category->term_id ) . '"  style="display: inline-block; vertical-align: middle !important;"/><div class="cat_name">'. $category->cat_name .'</div></a>
						';
						}
						} ?>
					</div>
					
				</div>
				
				
			</div>
		</div>
	</div>
</div>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="container">
				<div class="row as-post-featured outside">
					
					<?php
					$args = array(
					'post_type' => 'post',
					'posts_per_page' => 1,
					'tax_query' => array(
					array(
					'taxonomy' => 'category',
					'field'    => 'slug',
					'terms'    => 'featured',
					),
					),
					);
					$loop_blog = new WP_Query( $args );
					if ( $loop_blog->have_posts() ) {
					while ( $loop_blog->have_posts() ) : $loop_blog->the_post();
					get_template_part( '/template-parts/content', 'featuredpost'  );
					endwhile;
					} else {
					echo __( 'No posts were found' );
					}
					wp_reset_postdata();
					?>
				</div>
				
			</div>
			
			<div class="container">
				<div class="row as-post-section outside">
					<div class="card-columns">
						<!-- query -->
						<?php
						$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
						$query = new WP_Query( array(
						'post_type' => 'post',
						//'posts_per_page' => 3,
						'paged' => $paged,
						'category__not_in' => 511,
						) );
						?>
						
						<?php if ( $query->have_posts() ) : ?>
						<!-- begin loop -->
						<?php while ( $query->have_posts() ) : $query->the_post(); ?>
						<?php get_template_part( '/template-parts/content', 'post'  ); ?>
						<?php endwhile; ?>
						<!-- end loop -->
						<?php wp_reset_postdata(); ?>
						<?php else : ?>
						<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-md-12 as-pagination">
						<div class="pagination">
							<?php
							echo paginate_links( array(
							'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
							'total'        => $query->max_num_pages,
							'current'      => max( 1, get_query_var( 'paged' ) ),
							'format'       => '?paged=%#%',
							'show_all'     => false,
							'type'         => 'plain',
							'end_size'     => 2,
							'mid_size'     => 1,
							'prev_next'    => true,
							'prev_text'    => sprintf( '<i></i> %1$s', __( 'PREV PAGE', 'no-dig' ) ),
							'next_text'    => sprintf( '%1$s <i></i>', __( 'NEXT PAGE', 'no-dig' ) ),
							'add_args'     => false,
							'add_fragment' => '',
							) );
							?>
						</div>
					</div>
				</div>
			</div>
			</main><!-- #main -->
			</div><!-- #primary -->
			<?php
			get_footer();