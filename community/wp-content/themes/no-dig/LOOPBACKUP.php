<div id="blogCarousel" class="carousel slide" data-ride="carousel">
	<!-- Carousel items -->
	<div class="carousel-inner">
		
		
		
		<?php
		$args = array(
		'orderby' => 'name',
		'order' => 'ASC'
		);
		$categories = get_categories($args);
		foreach($categories as $category) {
		// check if image field is set on category
		if ( get_field( 'category_image', 'category_' . $category->term_id ) ) {
		// display link
		echo '<div class="carousel-item"><a class="cover margin-ultimos" href="' . get_category_link( $category->term_id ) . '"><div id="cover-home" class="gray-shadow"><img src="'. get_field( 'category_image', 'category_'.$category->term_id ). '" alt="Portada'. get_cat_name ( $category->term_id ) . '"  style="display: inline-block; vertical-align: middle !important;"/><div class="cat_name">'. $category->cat_name .'</div></div></a></div>
		';
		}
		} ?>
		
		
		
		<!--.row-->
		
		
	</div>
	<!--.carousel-inner-->
	<a class="carousel-control-prev" href="#blogCarousel" role="button" data-slide="prev">
		<span class="carousel-control-prev-icon" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
	</a>
	<a class="carousel-control-next" href="#blogCarousel" role="button" data-slide="next">
		<span class="carousel-control-next-icon" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
	</a>
</div>
<!--.Carousel-->
</div>