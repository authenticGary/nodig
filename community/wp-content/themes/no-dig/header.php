<?php
/**
* The header for our theme
*
* This is the template that displays all of the <head> section and everything up until <div id="content">
	*
	* @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
	*
	* @package No_Dig
	*/
	?>
	<!doctype html>
	<html <?php language_attributes(); ?>>
		<head>
			<meta charset="<?php bloginfo( 'charset' ); ?>">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<link rel="profile" href="https://gmpg.org/xfn/11">
			<?php wp_head(); ?>
		</head>
		<body <?php body_class(); ?>>
			<div id="page" class="site">
			 <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'no-dig' ); ?></a>

			 <?php 

             $detect = new Mobile_Detect;
             $deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');
             $scriptVersion = $detect->getScriptVersion();

			 if ( $detect->isMobile() && !$detect->isTablet() ) {
                get_template_part('template-parts/header', 'mobile');
             } else {
             	get_template_part('template-parts/header', 'desktop');
             }

			 

			 ?>



		<div id="content" class="site-content">