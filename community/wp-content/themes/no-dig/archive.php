<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package No_Dig
 */

get_header();
?>

<div class="container-fluid">
	<div class="row">
		<div class="col header_image" style="padding: 0px;">
			<?php
			$default_header_image = get_field('default_header_image', 'option');
			if (!empty($default_header_image)) { ?>
			<img src="<?php the_field('default_header_image', 'option');?>" alt="">
			<?php   }
			?>
			
		</div>
	</div>
</div>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			
			<div class="container as-archive-default">
				<div class="row">



		<?php if ( have_posts() ) : ?>

				<div class="col-md-12">
					
					<header class="page-header">
							<?php
							the_archive_title( '<h2 class="page-title">', '</h2>' );
							the_archive_description( '<div class="archive-description">', '</div>' );
							?>
					</header><!-- .page-header -->
			      		
	            </div>



			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', get_post_type() );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

				</div><!-- Row-->
			</div><!-- Container-->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
