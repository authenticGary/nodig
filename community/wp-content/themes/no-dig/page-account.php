<?php
/**
 * Template Name: Account
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package No_Dig
 */

get_header();
?>
<div class="container-fluid">
	<div class="row">
		<div class="col header_image" style="padding: 0px;">
			<?php
				$header_image = get_field('default_header_image', 'option');
			if (!empty($header_image)) { ?>
			<img src="<?php the_field('default_header_image', 'option');?>" alt="">
			<?php 	}
			?>
			
		</div>
	</div>
</div>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<div class="container">
				<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'account' );

		endwhile; // End of the loop.
		?>

			</div>

		
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
