<?php
/**
 * No Dig functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package No_Dig
 */

$files = new \FilesystemIterator( __DIR__.'/functions', \FilesystemIterator::SKIP_DOTS );
foreach ( $files as $file ) {
	/** @noinspection PhpIncludeInspection */
	! $files->isDir() and include $files->getRealPath();
}


