<?php
/**
* The template for displaying 404 pages (not found)
*
* @link https://codex.wordpress.org/Creating_an_Error_404_Page
*
* @package No_Dig
*/
get_header();
?>
<style type="text/css" media="screen">
	.as-four-o-four {
		text-align: center;
		margin-top: 30px;
		margin-bottom: 30px;
	}
	.error-404.not-found {
		width:  100%;
	}
	input.search-submit {
		color:  #fff;
    background-color: #66883f;
    border-color: #66883f;
	display: inline-block;
    font-weight: 400;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    user-select: none;
    border: 1px solid transparent;
    padding: .375rem .75rem;
    font-size: 1rem;
    line-height: 1.5;
    border-radius: .25rem;
    transition: color 0.15s ease-in-out,background-color 0.15s ease-in-out,border-color 0.15s ease-in-out,box-shadow 0.15s ease-in-out;
	}
	input.search-field {
		height:  40px;
	}
</style>
<div class="container-fluid">
	<div class="row">
		<div class="col header_image" style="padding: 0px;">
			<?php
			$default_header_image = get_field('default_header_image', 'option');
			if (!empty($default_header_image)) { ?>
			<img src="<?php the_field('default_header_image', 'option');?>" alt="">
			<?php   }
			?>
			
		</div>
	</div>
</div>
<div id="primary" class="content-area">
	<main id="main" class="site-main">
		<div class="container as-four-o-four">
			<div class="row">
				<section class="error-404 not-found">
					<header class="page-header">
						<h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'no-dig' ); ?></h1>
						</header><!-- .page-header -->
						
						<div class="page-content">
							<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'no-dig' ); ?></p>
							<div class="row">
								<div class="col-md-12">
									<?php
									get_search_form();
									?>
								</div>
								
							</div>
							
							</div><!-- .page-content -->
							</section><!-- .error-404 -->
						</div>
					</div>
					
					</main><!-- #main -->
					</div><!-- #primary -->
					<?php
					get_footer();