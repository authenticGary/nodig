<?php
function nodig_custom_scripts() {
	// Linear icon font
	wp_enqueue_style('linear-icons', 'https://cdn.linearicons.com/free/1.0.0/icon-font.min.css');
	wp_enqueue_style('bootstrapcss', get_template_directory_uri() . '/css/bootstrap.css');
	wp_enqueue_style('mediacss', get_template_directory_uri() . '/css/media_queries.css');
	wp_enqueue_style('fontawesomecss', get_template_directory_uri() . '/css/all.css');
	wp_enqueue_style('owlcss', get_template_directory_uri() . '/vendors/owl/assets/owl.carousel.css');
	wp_enqueue_style('stylecss', get_template_directory_uri() . '/css/style.css');
	wp_enqueue_style('custommcss', get_template_directory_uri() . '/css/custom.css');
	wp_enqueue_style('lightboxcss', get_template_directory_uri() . '/lightbox/css/lightbox.css');
	wp_enqueue_script('jqueryjs', get_template_directory_uri() . '/js/jquery.js', array('jquery'));
	wp_enqueue_script('bootstrapjs', get_template_directory_uri() . '/js/bootstrap.js', array('jquery'), 1, true);
	wp_enqueue_script('mainjs', get_template_directory_uri() . '/js/main.js', array('jquery'), 1, true);
	wp_enqueue_script('owljs', get_template_directory_uri() . '/vendors/owl/js/owl.carousel.js', array('jquery'), 1, true);
	wp_enqueue_script('google-map', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBb5p3HMjw1VJSsCt2L9qkResGYmmArWuk', array(), '3', true);
	wp_enqueue_script('mapjs', get_template_directory_uri() . '/assets/js/google-maps.js', array('jquery'), 1, true);
	wp_enqueue_script('lightboxjs', get_template_directory_uri() . '/lightbox/js/lightbox.js', array('jquery'), 1, true);
	wp_enqueue_script('sticky-sidebar-stop', get_template_directory_uri() . '/js/sticky-sidebar-stop.js', array('jquery'), 1, true);	
}
add_action('wp_enqueue_scripts', 'nodig_custom_scripts');

/**
 * @snippet       WooCommerce Hide Prices on the Shop Page
 * @how-to        Watch tutorial @ https://businessbloomer.com/?p=19055
 * @sourcecode    https://businessbloomer.com/?p=406
 * @author        Rodolfo Melogli
 * @compatible    WooCommerce 3.4.3
 */

add_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10);

// define the woocommerce_product_thumbnails callback
function action_woocommerce_product_thumbnails() {
	do_action('woocommerce_shop_loop_item_title');
};

// add the action
add_action('woocommerce_product_thumbnails', 'action_woocommerce_product_thumbnails', 10, 0);

// Google Map

function my_acf_init() {

	acf_update_setting('google_api_key', 'AIzaSyBb5p3HMjw1VJSsCt2L9qkResGYmmArWuk');
}

add_action('acf/init', 'my_acf_init');

//Excerpt

function excerpt($num) {
	$limit = $num + 1;
	$excerpt = explode(' ', get_the_excerpt(), $limit);
	array_pop($excerpt);
	echo $excerpt;
}


add_filter( 'get_custom_logo', 'wecodeart_com' );
function wecodeart_com() {
    $custom_logo_id = get_theme_mod( 'custom_logo' );
    $html = sprintf( '<a href="%1$s" class="custom-logo-link" rel="home" itemprop="url">%2$s</a>',
            esc_url( '/' ),
            wp_get_attachment_image( $custom_logo_id, 'full', false, array(
                'class'    => 'custom-logo',
            ) )
        );
    return $html;   
} 