<?php


add_action('get_header', 'remove_storefront_sidebar');
function remove_storefront_sidebar() {
	if (is_woocommerce()) {
		remove_action('storefront_sidebar', 'storefront_get_sidebar', 10);
	}
}
// Change number or products per row to 4
add_filter('loop_shop_columns', 'loop_columns');
if (!function_exists('loop_columns')) {
	function loop_columns() {
		return 4; // 4 products per row
	}
}

// Remove Bread Crumbs
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);
// Remove Title
add_filter('woocommerce_show_page_title', 'woo_hide_page_title');
/**
 * woo_hide_page_title
 *
 * Removes the "shop" title on the main shop page
 *
 * @access      public
 * @since       1.0
 * @return      void
 */
function woo_hide_page_title() {

	return false;
}
/*
// Add description to product
add_filter('woocommerce_cart_item_name', 'cart_description', 20, 3);
function cart_description($name, $cart_item, $cart_item_key) {
// Get the corresponding WC_Product
	$product_item = $cart_item['data'];
	if (!empty($product_item)) {
// WC 3+ compatibility
		$description = $product_item->get_description();
		$result = __('Description: ', 'woocommerce') . $description;
		return $name . '<br>' . $result;
	} else {
		return $name;
	}

}
*/

// Change Shipping Name

add_filter('gettext', 'translate_reply');
add_filter('ngettext', 'translate_reply');

function translate_reply($translated) {
	$translated = str_ireplace('Shipping', 'Postage', $translated);
	return $translated;
}

// Add Custom button text
add_filter('woocommerce_product_add_to_cart_text', 'custom_woocommerce_product_add_to_cart_text');
function custom_woocommerce_product_add_to_cart_text() {
	global $product;
	if ($product_type = $product->is_type('variable')) {
		return __('Find out more', 'woocommerce');
	} else {
		return __('Buy Now', 'woocommerce');
	}
}
function wc_custom_single_addtocart_text() {
	return "ADD TO BASKET";
}
add_filter('woocommerce_product_single_add_to_cart_text', 'wc_custom_single_addtocart_text');


// Move payment method

remove_action( 'woocommerce_checkout_order_review', 'woocommerce_checkout_payment', 20 );
add_action( 'woocommerce_after_order_notes', 'woocommerce_checkout_payment', 20 );


/**
* @snippet Remove Order Notes (fully, including the title) on the WooCommerce Checkout
* @how-to Watch tutorial @ https://businessbloomer.com/?p=19055
* @sourcecode https://businessbloomer.com/?p=17432
* @author Rodolfo Melogli
* @testedwith WooCommerce 3.4.4
*/

add_filter( 'woocommerce_enable_order_notes_field', '__return_false' );

// Checkoutcoupon and login
remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 );
add_action( 'woocommerce_checkout_after_order_review', 'woocommerce_checkout_coupon_form' );

remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_login_form', 10 );
add_action( 'woocommerce_checkout_after_order_review', 'woocommerce_checkout_login_form' );


// Move Upsells

/**
* @snippet Move & Change Number of Cross-Sells @ WooCommerce Cart
* @how-to Watch tutorial @ https://businessbloomer.com/?p=19055
* @sourcecode https://businessbloomer.com/?p=20449
* @author Rodolfo Melogli
* @testedwith WooCommerce 2.6.2
*/
 
 
// ---------------------------------------------
// Remove Cross Sells From Default Position 
 
remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display' );
 
 
// ---------------------------------------------
// Add them back UNDER the Cart Table
 
add_action( 'woocommerce_after_cart_table', 'woocommerce_cross_sell_display' );
 
 
// ---------------------------------------------
// Display Cross Sells on 3 columns instead of default 4
 
add_filter( 'woocommerce_cross_sells_columns', 'bbloomer_change_cross_sells_columns' );
 
function bbloomer_change_cross_sells_columns( $columns ) {
return 3;
}
 
 
// ---------------------------------------------
// Display Only 3 Cross Sells instead of default 4
 
add_filter( 'woocommerce_cross_sells_total', 'bbloomer_change_cross_sells_product_no' );
  
function bbloomer_change_cross_sells_product_no( $columns ) {
return 3;
}




