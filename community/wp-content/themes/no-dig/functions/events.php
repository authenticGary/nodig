<?php 


if ( ! function_exists('custom_events') ) {

// Register Custom Post Type
function custom_events() {

	$labels = array(
		'name'                  => _x( 'Events', 'Post Type General Name', 'no-dig' ),
		'singular_name'         => _x( 'Events', 'Post Type Singular Name', 'no-dig' ),
		'menu_name'             => __( 'Events', 'no-dig' ),
		'name_admin_bar'        => __( 'Events', 'no-dig' ),
		'archives'              => __( 'Events Archives', 'no-dig' ),
		'attributes'            => __( 'events Attributes', 'no-dig' ),
		'parent_item_colon'     => __( 'Parent events:', 'no-dig' ),
		'all_items'             => __( 'All events', 'no-dig' ),
		'add_new_item'          => __( 'Add New events', 'no-dig' ),
		'add_new'               => __( 'Add New', 'no-dig' ),
		'new_item'              => __( 'New Item', 'no-dig' ),
		'edit_item'             => __( 'Edit Item', 'no-dig' ),
		'update_item'           => __( 'Update Item', 'no-dig' ),
		'view_item'             => __( 'View Item', 'no-dig' ),
		'view_items'            => __( 'View Items', 'no-dig' ),
		'search_items'          => __( 'Search Item', 'no-dig' ),
		'not_found'             => __( 'Not found', 'no-dig' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'no-dig' ),
		'featured_image'        => __( 'Featured Image', 'no-dig' ),
		'set_featured_image'    => __( 'Set featured image', 'no-dig' ),
		'remove_featured_image' => __( 'Remove featured image', 'no-dig' ),
		'use_featured_image'    => __( 'Use as featured image', 'no-dig' ),
		'insert_into_item'      => __( 'Insert into item', 'no-dig' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'no-dig' ),
		'items_list'            => __( 'Items list', 'no-dig' ),
		'items_list_navigation' => __( 'Items list navigation', 'no-dig' ),
		'filter_items_list'     => __( 'Filter items list', 'no-dig' ),
	);
	$args = array(
		'label'                 => __( 'Events', 'no-dig' ),
		'description'           => __( 'Events Description', 'no-dig' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'page-attributes' ),
		'taxonomies'            => array( 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'menu_icon'             => 'dashicons-calendar-alt',
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'events', $args );

}
add_action( 'init', 'custom_events', 0 );

}


if ( ! function_exists( 'events_taxonomy' ) ) {

// Register Custom Taxonomy
function events_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Events', 'Taxonomy General Name', 'no-dig' ),
		'singular_name'              => _x( 'Events', 'Taxonomy Singular Name', 'no-dig' ),
		'menu_name'                  => __( 'Events Tax', 'no-dig' ),
		'all_items'                  => __( 'All Items', 'no-dig' ),
		'parent_item'                => __( 'Parent Item', 'no-dig' ),
		'parent_item_colon'          => __( 'Parent Item:', 'no-dig' ),
		'new_item_name'              => __( 'New Item Name', 'no-dig' ),
		'add_new_item'               => __( 'Add New Item', 'no-dig' ),
		'edit_item'                  => __( 'Edit Item', 'no-dig' ),
		'update_item'                => __( 'Update Item', 'no-dig' ),
		'view_item'                  => __( 'View Item', 'no-dig' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'no-dig' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'no-dig' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'no-dig' ),
		'popular_items'              => __( 'Popular Items', 'no-dig' ),
		'search_items'               => __( 'Search Items', 'no-dig' ),
		'not_found'                  => __( 'Not Found', 'no-dig' ),
		'no_terms'                   => __( 'No items', 'no-dig' ),
		'items_list'                 => __( 'Items list', 'no-dig' ),
		'items_list_navigation'      => __( 'Items list navigation', 'no-dig' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'events', array( 'events' ), $args );

}
add_action( 'init', 'events_taxonomy', 0 );

}



function custom_field_excerpt() {
	global $post;
	$text = get_field('event_details'); //Replace 'your_field_name'
	if ( '' != $text ) {
		$text = strip_shortcodes( $text );
		$text = apply_filters('the_content', $text);
		$text = str_replace(']]&gt;', ']]&gt;', $text);
		$excerpt_length = 20; // 20 words
		$excerpt_more = apply_filters('excerpt_more', ' ' . '[...]');
		$text = wp_trim_words( $text, $excerpt_length, $excerpt_more );
	}
	return apply_filters('the_excerpt', $text);
}