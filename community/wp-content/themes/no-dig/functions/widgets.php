<?php
/**
 * Register our sidebars and widgetized areas.
 *
 */
function social_widgets_init() {

	register_sidebar( array(
		'name'          => 'Social Icons',
		'id'            => 'social_icons',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded">',
		'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'social_widgets_init' );

// Footer Widget Area 1

function footer_one_widgets_init() {

	register_sidebar( array(
		'name'          => 'Footer One',
		'id'            => 'footer_one',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded">',
		'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'footer_one_widgets_init' );

// Footer Widget Area 2

function footer_two_widgets_init() {

	register_sidebar( array(
		'name'          => 'Footer Two',
		'id'            => 'footer_two',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded">',
		'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'footer_two_widgets_init' );

// Footer Widget Area 3

function footer_three_widgets_init() {

	register_sidebar( array(
		'name'          => 'Footer Three',
		'id'            => 'footer_three',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded">',
		'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'footer_three_widgets_init' );

// Footer Widget Area 4

function footer_four_widgets_init() {

	register_sidebar( array(
		'name'          => 'Footer Four',
		'id'            => 'footer_four',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded">',
		'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'footer_four_widgets_init' );

// Footer Widget Area 5

function footer_five_widgets_init() {

	register_sidebar( array(
		'name'          => 'Footer Five',
		'id'            => 'footer_five',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded">',
		'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'footer_five_widgets_init' );

// Footer Widget Area 6

function footer_six_widgets_init() {

	register_sidebar( array(
		'name'          => 'Footer Six',
		'id'            => 'footer_six',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded">',
		'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'footer_six_widgets_init' );




// Login Page Widget

function login_page_widgets_init() {

	register_sidebar( array(
		'name'          => 'Login Page',
		'id'            => 'login_page',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded">',
		'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'login_page_widgets_init' );


