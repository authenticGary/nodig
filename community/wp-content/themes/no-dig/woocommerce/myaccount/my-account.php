<?php
/**
 * My Account page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

defined( 'ABSPATH' ) || exit;

/**
 * My Account navigation.
 *
 * @since 2.6.0 
 */
?>
<div class="container-fluid">
  <div class="row">
    <div class="col header_image" style="padding: 0px;">
      <?php
      $default_header_image = get_field('default_header_image', 'option');
      if (!empty($default_header_image)) { ?>
      <img src="<?php the_field('default_header_image', 'option');?>" alt="">
      <?php   }
      ?>
      
    </div>
  </div>
</div>

<div class="container as-my-account">
	<div class="row outside">
		<?php do_action( 'woocommerce_account_navigation' ); ?>

<div class="col-md-9 woocommerce-MyAccount-content">
	<?php
		/**
		 * My Account content.
		 *
		 * @since 2.6.0
		 */
		do_action( 'woocommerce_account_content' );
	?>
</div>
	</div>
</div>

