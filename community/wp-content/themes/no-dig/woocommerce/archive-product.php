<?php
/**
* The Template for displaying product archives, including the main shop page which is a post type archive
*
* This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
*
* HOWEVER, on occasion WooCommerce will need to update template files and you
* (the theme developer) will need to copy the new files to your theme to
* maintain compatibility. We try to do this as little as possible, but it does
* happen. When this occurs the version of the template file will be bumped and
* the readme will list any important changes.
*
* @see https://docs.woocommerce.com/document/template-structure/
* @package WooCommerce/Templates
* @version 3.4.0
*/
defined( 'ABSPATH' ) || exit;
get_header( 'shop' ); ?>
<?php
$detect = new Mobile_Detect;
$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');
$scriptVersion = $detect->getScriptVersion();
if ( $detect->isMobile() && !$detect->isTablet() ) { ?>
<div class="col-md-12 as-mobile-shop-carousel">
	
	<div class="owl-carousel">
		
		<?php
			// check if the repeater field has rows of data
			if( have_rows('categories_header', 10) ):
				// loop through the rows of data
		while ( have_rows('categories_header', 10) ) : the_row(); ?>
		<?php
		$post_object = get_sub_field('category_link', 10);
		$postURL = get_category_link($post_object[0]);
		?>
		<div>
			
			
				<a href="<?php echo $postURL; ?>">
					<img src="<?php the_sub_field('category_image', 10)?>">
					
				</a>
				<div class="cat_name">
					<div class="cat_title"><?php the_sub_field('category_name', 10)?></div>
				</div>
			
			
		</div>
		<?php    endwhile;
		else :
		// no rows found
		endif;
		?>
	</div>
</div>

<?php } else { ?>
<div class="container-fluid as-shop-header">
	<div class="container">
		<div class="row outside">
			<?php
				// check if the repeater field has rows of data
				if( have_rows('categories_header', 10) ):
					// loop through the rows of data
			while ( have_rows('categories_header', 10) ) : the_row(); ?>
			<?php
			$post_object = get_sub_field('category_link', 10);
			$postURL = get_category_link($post_object[0]);
			?>
			
			<div class="col-md-4 as-shop-cat-header">
				<a href="<?php echo $postURL; ?>">
					<img src="<?php the_sub_field('category_image', 10)?>">
					
				</a>
				<div class="cat_name">
					<div class="cat_title"><?php the_sub_field('category_name', 10)?></div></div>
				</div>
				<?php    endwhile;
				else :
				// no rows found
				endif;
				?>
			</div>
		</div>
	</div>
	
	<?php } ?>
	<div class="container">
		<div class="row">
			<?php /**
			* Hook: woocommerce_before_main_content.
			*
			* @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
			* @hooked woocommerce_breadcrumb - 20
			* @hooked WC_Structured_Data::generate_website_data() - 30
			*/
			do_action( 'woocommerce_before_main_content' );
			?>
			<div class="col-md-12">
			<header class="woocommerce-products-header">

				<span class="back-to-shop-link" style="display: none;">
					<a href="/shop" target="_self">BACK TO SHOP</a>
				</span>

				<?php if ( !apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
				<h1 class="woocommerce-products-header__title page-title"><?php woocommerce_page_title(); ?></h1>
				<?php endif; ?>

				</div>
				<?php
				/**
				* Hook: woocommerce_archive_description.
				*
				* @hooked woocommerce_taxonomy_archive_description - 10
				* @hooked woocommerce_product_archive_description - 10
				*/
				do_action( 'woocommerce_archive_description' );
				?>
			</header>
			<?php
			if ( woocommerce_product_loop() ) {
				/**
				* Hook: woocommerce_before_shop_loop.
				*
				* @hooked woocommerce_output_all_notices - 10
				* @hooked woocommerce_result_count - 20
				* @hooked woocommerce_catalog_ordering - 30
				*/
				//do_action( 'woocommerce_before_shop_loop' );
				woocommerce_product_loop_start();
				if ( wc_get_loop_prop( 'total' ) ) {
					while ( have_posts() ) {
						the_post();
						/**
						* Hook: woocommerce_shop_loop.
						*
						* @hooked WC_Structured_Data::generate_product_data() - 10
						*/
						do_action( 'woocommerce_shop_loop' );
						wc_get_template_part( 'content', 'product' );
					}
				}
				woocommerce_product_loop_end();
				/**
				* Hook: woocommerce_after_shop_loop.
				*
				* @hooked woocommerce_pagination - 10
				*/
				do_action( 'woocommerce_after_shop_loop' );
			} else {
				/**
				* Hook: woocommerce_no_products_found.
				*
				* @hooked wc_no_products_found - 10
				*/
				do_action( 'woocommerce_no_products_found' );
			}
			/**
			* Hook: woocommerce_after_main_content.
			*
			* @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
			*/
			do_action( 'woocommerce_after_main_content' ); ?>
			
		</div>
	</div>
	<div class="container-fluid as-shop-header">
		<div class="container h-100">
			<div class="row align-items-center h-100">
				<div class="col-md-6 "> <!-- Testimonial Block -->
				<div class="col-md-12 as-front-testimonial">
					<div class="row as-slider-item">
						<?php get_template_part( '/template-parts/content', 'testimonial'  );?>
					</div>
				</div>
				
				</div> <!-- /Testimonial Block -->
				<div class="col-md-6">
					
					<!-- Owl Carousel Repeater -->
					<?php if( have_rows('logos', 10) ): ?>
					
					<div class="col-md-12 as-as-seen-shop">
						<div class="col-md-12 carousel_title">
							<h3 class="text-center">As Seen In</h3>
						</div>
						
						<div class="owl-carousel">
							<?php while( have_rows('logos', 10) ): the_row();  ?>
							<div>
								
								<div class="col-md-12 as-top-logos">
									<img src="<?php the_sub_field('logo_top', 10);?>" style="display: inline-block; vertical-align: middle !important; width: 150px !important;">
								</div>
								<div class="col-md-12 as-bottom-logos">
									<img src="<?php the_sub_field('logo_bottom', 10);?>" style="display: inline-block; vertical-align: middle !important; width: 150px !important;">
								</div>
								
								
							</div>
							<?php endwhile; ?>
						</div>
						<?php endif; ?>
						<!-- Owl Carousel -->
					</div>
				</div>
			</div>
		</div>
		<?php
		/**
		* Hook: woocommerce_sidebar.
		*
		* @hooked woocommerce_get_sidebar - 10
		*/
		do_action( 'woocommerce_sidebar' );
		get_footer( 'shop' );