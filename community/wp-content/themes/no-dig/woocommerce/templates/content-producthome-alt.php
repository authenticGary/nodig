<div class="col col-sm-6 col-md-3 products-single">
	
		<div class="col-md-12 product-box">
			<?php do_action( 'woocommerce_before_shop_loop_item_title', 'large' ); ?>
		
			<?php do_action( 'woocommerce_product_thumbnails' );?>
			<div class="col-md-12 front_page_product_price">
				<?php
				global $woocommerce;
				$currency = get_woocommerce_currency_symbol();
				$price = get_post_meta( get_the_ID(), '_regular_price', true);
				$sale = get_post_meta( get_the_ID(), '_sale_price', true);
				?>
				
				<?php if($sale) : ?>
				<p class="product-price-tickr"><del><?php echo $currency; echo $price; ?></del> <?php echo $currency; echo $sale; ?></p>
				<?php elseif($price) : ?>
				<p class="product-price-tickr"><?php echo $currency; echo $price; ?></p>
				<?php endif; ?>
				<div class="col-md-12 as-find-button">
					<?php 	do_action( 'woocommerce_after_shop_loop_item' ); ?>
				</div>
			</div>
		
		</div>
	
</div>