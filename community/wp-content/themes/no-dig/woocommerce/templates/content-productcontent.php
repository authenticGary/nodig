<div class="row home_products">
	
<div class="col-md-12 as-product-bg">
<div class="row">	
<div class="col-md-4 as-product-image">
	<?php do_action( 'woocommerce_before_shop_loop_item_title', 'large' ); ?>
</div>
<div class="col-md-8 as-product-description">
<?php do_action( 'woocommerce_product_thumbnails' ); ?>

<?php
$product = get_sub_field('single_product');
add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
?>

<div class="col-md-12 as-find-button">
<?php 	do_action( 'woocommerce_after_shop_loop_item' ); ?>
</div>
</div>
</div>
</div>
</div>
