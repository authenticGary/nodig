<div class="row home_products">
	
<div class="col-md-12 as-product-bg">
<div class="row">	
<div class="col-md-6 as-product-image">
	<img src="<?php the_field('course_image');?>">
</div>
<div class="col-md-6 as-product-description">
	<?php
	do_action( 'woocommerce_product_thumbnails' );
			if (is_front_page()) {
			global $product;
			$count_in_stock = 0;
			if ( $product->is_type( 'variable' ) ) {
			$variation_ids = $product->get_children(); // Get product variation IDs
			foreach( $variation_ids as $variation_id ){
			$variation = wc_get_product($variation_id);
			if( $variation->is_in_stock() )
			$count_in_stock++;
			}
			}
			// Your condition
			if( $count_in_stock >= 1 ){
			echo $count_in_stock . " Dates Available";
			} else {
				echo "No course dates avaliable";
			}
			} ?>

<div class="col-md-12 as-find-button">
<?php 	do_action( 'woocommerce_after_shop_loop_item' ); ?>
</div>
</div>
</div>
</div>
</div>
