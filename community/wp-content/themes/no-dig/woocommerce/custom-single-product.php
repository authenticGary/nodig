<?php
/**
* The template for displaying product content in the single-product.php template
*
* This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
*
* HOWEVER, on occasion WooCommerce will need to update template files and you
* (the theme developer) will need to copy the new files to your theme to
* maintain compatibility. We try to do this as little as possible, but it does
* happen. When this occurs the version of the template file will be bumped and
* the readme will list any important changes.
*
* @see     https://docs.woocommerce.com/document/template-structure/
* @package WooCommerce/Templates
* @version 3.4.0
*/
defined('ABSPATH') || exit;
/**
* Hook: woocommerce_before_single_product.
*
* @hooked wc_print_notices - 10
*/
do_action('woocommerce_before_single_product');
if (post_password_required()) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>
<div id="product-<?php the_ID();?>" <?php wc_product_class();?>>
	<div class="container as-single-product">
		<div class="row">
			<div class="col-md-8 as-single-product-left">
				<div class="col-md-12 as-single-title">
					<span class="back-to-shop-link">
						<a href="/shop" target="_self">BACK TO SHOP</a>
					</span>
					<h2><?php the_title();?></h2>
					<?php
					$sub_headding = get_field('sub_heading');
					if (!empty($sub_headding)) {
					echo '<p class="small">' . $sub_headding . '</p>';
					}
					?>
				</div>
				<?php
				// check if the flexible content field has rows of data
				if (have_rows('single_product_content')):
				// loop through the rows of data
				while (have_rows('single_product_content')): the_row();?>
				<!-- Single Column Text -->
				<?php if (get_row_layout() == 'single_column_content'): ?>
				<div class="col-md-12 as-flexible-content">
					<?php the_sub_field('single_column_text');?>
				</div>
				<!-- Single Column Image -->
				<?php elseif (get_row_layout() == 'single_column_image'): ?>
				<div class="col-md-12 as-flexible-imge">
					<img src="<?php the_sub_field('single_column_image');?>" >
				</div>
				<!-- Single Column Video -->
				<?php elseif (get_row_layout() == 'you_tube_video'): ?>
				<div class="col-md-12 as-flexible-video">
					<?php
					$video_section = get_sub_field('you_tube_video');
					if (!empty($video_section)) { ?>
					
					
					<div class="embed-container">
						<?php
											// get iframe HTML
											$iframe = get_sub_field('you_tube_video');
											// use preg_match to find iframe src
											preg_match('/src="(.+?)"/', $iframe, $matches);
											$src = $matches[1];
											// add extra params to iframe src
											$params = array(
											'controls'    => 1,
											'hd'        => 1,
											'autohide'    => 1
											);
											$new_src = add_query_arg($params, $src);
											$iframe = str_replace($src, $new_src, $iframe);
											// add extra attributes to iframe html
											$attributes = 'frameborder="0"';
											$iframe = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $iframe);
											// echo $iframe
											echo $iframe;
						?>
						<style>
							.embed-container {
								position: relative;
								padding-bottom: 56.25%;
								overflow: hidden;
								max-width: 100%;
								height: auto;
								border-radius: 10px;
							}
							.embed-container iframe,
							.embed-container object,
							.embed-container embed {
								position: absolute;
								top: 0;
								left: 0;
								width: 100%;
								height: 100%;
							}
						</style>
					</div>
					<?php 	} ?>
				</div>
				<!-- Single Column gallery -->
				<?php elseif (get_row_layout() == 'gallery'): ?>
				<!-- The Gallery -->
				<div class="col-md-12 as-content-section as-shop-gallery-section">
					<div class="row">
						
						
						<?php
										$images = get_sub_field('gallery');
										$size = 'full'; // (thumbnail, medium, large, full or custom size)
						if( $images ): ?>
						<?php foreach( $images as $image ): ?>
						<div class="col-6 col-md-3 as-single-shop-image">
							
								<?php
								$content = '<a class="gallery_image popup" href="'. $image['url'] .'" data-lightbox="roadtrip">';
								//$content .= '<img src="'. $image['sizes']['thumbnail'] .'" alt="'. $image['alt'] .'" />';
								$content .= '<div class="col-md-12 alm-gallery-img single-shop-gallery" style="background-image: url('.$image['url'].')">';
								$content .= '</div>';
								$content .= '<i class="fas fa-search"></i></a>';
								if ( function_exists('slb_activate') ){
								$content = slb_activate($content);
								}
								echo $content;
								?>

								
							
						</div>
						<?php endforeach; ?>
						
						<?php endif; ?>
						
						
						
					</div>
					
				</div>
				<!-- Single Column Image -->
				<?php elseif (get_row_layout() == 'testimonial'): ?>
				<div class="col-md-12 as-front-testimonial as-shop-testimonial">
					<div class="row as-slider-item h-100">
						<?php get_template_part( '/template-parts/content', 'testimonial-shop'  );?>
					</div>
				</div>
				<!-- More details 1-->
				<?php elseif (get_row_layout() == 'course_details'): ?>
				<div class="col-md-12 as-course_details">
					<div class="col-md-12">
						<h6>More Details</h6>
					</div>
					<!-- address Section -->
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-2">
								Time:
							</div>
							<div class="col-md-10">
								<?php the_sub_field('start_time'); ?> - <?php the_sub_field('end_time'); ?>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-2">
								Venue:
							</div>
							<div class="col-md-10">
								<?php the_sub_field('venue');?>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-2">
								Phone:
							</div>
							<div class="col-md-10">
								<?php the_sub_field('phone_number');?>
							</div>
						</div>
					</div>
					
				</div>
				<?php elseif (get_row_layout() == 'more_details'): ?>
				<div class="col-md-12 as-course_details">
					<div class="col-md-12">
						<h6>More Details</h6>
					</div>
					<div class="col-md-12">
						<div class="col-md-12 as-more-info">
							<?php the_sub_field('more_detail_text');?>
						</div>
						<div class="col-md-12 as-more-info">
							<div class="row">
								<div class="col-md-1">
									Weight:
								</div>
								<div class="col-md-10">
									<?php the_sub_field('weight');?> kg
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php	endif;?>
				<?php endwhile;
				else:
					// no layouts found
				endif;
				?>
			</div>
			<!--  Product Container -->
			<div class="col-md-4 mh-75 as-single-product-right">
				<?php
				/**
				* Hook: woocommerce_before_single_product_summary.
				*
				* @hooked woocommerce_show_product_sale_flash - 10
				* @hooked woocommerce_show_product_images - 20
				*/
				do_action('woocommerce_before_single_product_summary');
				?>
				<div class="summary entry-summary">
					<?php
					/**
					* Hook: woocommerce_single_product_summary.
					*
					* @hooked woocommerce_template_single_title - 5
					* @hooked woocommerce_template_single_rating - 10
					* @hooked woocommerce_template_single_price - 10
					* @hooked woocommerce_template_single_excerpt - 20
					* @hooked woocommerce_template_single_add_to_cart - 30
					* @hooked woocommerce_template_single_meta - 40
					* @hooked woocommerce_template_single_sharing - 50
					* @hooked WC_Structured_Data::generate_product_data() - 60
					*/
					do_action('woocommerce_single_product_summary');
					?>
				</div>
				<?php do_action('woocommerce_after_single_product'); ?>
				<div class="col-md-12 as-single-sider-review">
				<div class="row">
				<div id="accordion">
					<div class="card">
						<div class="card-header" id="headingOne">
							<h5 class="mt-20">
							<button class="btn btn-link text-center as-review-button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
							Have you purchased this product? <br>
							<span class="fake_link">Leave a review?</span>
							</button>
							</h5>
						</div>
						<div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
							<div class="card-body as-review-body">
								<?php do_action( 'woocommerce_after_single_product_summary', 'comments_template', 50 );?>
							</div>
						</div>
					</div>
				</div>
				</div>
				</div>
				
			</div>
		</div>
	</div>
</div>