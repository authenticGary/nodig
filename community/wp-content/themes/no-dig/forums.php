<?php
/**
 * The template for displaying all bbPress pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package No_Dig
 */

get_header();
?>

<div class="container-fluid">
	<div class="row">
		<div class="col header_image" style="padding: 0px;">
			<?php
			$page_header_image = get_field('page_header_image', 50731);
			if (!empty($page_header_image)) { ?>
			<img src="<?php the_field('page_header_image', 50731);?>" alt="">
			<?php 	}
			?>
			
		</div>
	</div>
</div>
 
<?php
/*
Surrounding Classes for the site
 
These are different every theme and help with structure and layout
 
These could be SPANs or DIVs and with entirely different classes.
*/
?>
 
<div id="primary" class="site-content">
 

<div class="container">
<div id="content" role="main">

<div class="row outside">

<?php
/*
Start the Loop
*/
?>
 
<?php while ( have_posts() ) : the_post(); ?>
 
 
<?php
/*
This is the start of the page and also the insertion of the post classes.
 
Post classes are very handy to style your forums.
*/
?>
<div class="col-md-8 as-forum-content">
	<?php if(is_user_logged_in()) { ?><!-- check if user is logged in-->
	<?php if(bbp_is_single_forum()){ ?><!-- check if is sing forum -->
	<a class="btn btn-primary as-new-topic" href="#new-post">CREATE TOPIC</a>
    <?php }?>	<!-- check if is sing forum -->
    <?php } ?><!-- check if user is logged in-->
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
 
 
<?php
/*
This is the title wrapped in a header tag
 
and a class to better style the title for your theme
*/
?>
 
<header class="entry-header">
 
<h1 class="entry-title"><?php the_title(); ?></h1>
 
</header>
 
 
<?php
/*
This is the content wrapped in a div
 
and class to better style the content
*/
?>
 
<div class="entry-content">
<?php the_content(); ?>
</div>
 
<!-- .entry-content -->
 
 
<?php
/*
End of Page
*/
?>
 
</article>
 </div>
 <div class="col-md-4 stikey_sidebar">
 				<div class="row">
 					<div class="col-md-12 as-forum-info"><h4>Forum Info</h4></div>
 				</div>
 			    
			 	<?php echo do_shortcode('[bbp-stats]');?>
			 	<div class="col-md-12 as-forum-search">
			 	<h6>Forum Search</h6>
			 	<?php echo do_shortcode('[bbp-search-form]');?>
			 	</div>
 </div>
<!-- #post -->
<?php endwhile; // end of the loop. ?>
</div>
</div><!-- #container -->
</div>
 
<!-- #content -->
 
</div>
 
<!-- #primary -->
 
 
<?php
/*
This is code to display the sidebar and the footer.
 
Remove the sidebar code to get full-width forums.
 
This would also need CSS to make it actually full width.
*/
?>

<?php
//get_sidebar();
get_footer();
