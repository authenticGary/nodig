<?php
/**
* The template for displaying archive events pages
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/
*
* @package No_Dig
*/
get_header();
?>
<div class="container-fluid">
	<div class="row">
		<div class="col" style="padding: 0px;">
			<?php
			$events_header_image = get_field('events_header_image', 'option');
			if (!empty($events_header_image)) { ?>
			<img src="<?php the_field('events_header_image', 'option');?>" alt="">
			<?php 	}
			?>
		</div>
	</div>
</div>
<div id="primary" class="content-area">
	<main id="main" class="site-main">
		<div class="container">
			<div class="row justify-content-md-center">
				<div class="col col-lg-8 col-lg-offset-2 as-events-area"><!-- content holder and flexible content -->
				<?php
				// $categories = get_terms( 'events', 'orderby=count' );
				//$terms = get_terms( 'events' );

				$terms = get_terms( 'events', array(
				    'orderby'    => 'menu',
				    'order' => 'DESC',
				    'hide_empty' => 1,
				) );



				if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
				foreach ( $terms as $term ) { ?>
				<div class="accordion" id="accordion<?php echo $term->name; ?>">
					<div class="card-header" id="headingOne">
						<h2 class="mb-0">
						<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse<?php echo $term->name?>" aria-expanded="true" aria-controls="collapseOne<?php echo $term->name?>">
						<?php  echo $term->name;?><i class="fas fa-angle-down"></i>
						</button>
						</h2>
					</div>
					<div id="collapse<?php echo $term->name?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordion<?php echo $term->name?>">
						<div class="card-body">
							<?php
							$event_cat = $term->name;
							$args = array(
							'post_type' => 'events',
							'posts_per_page' => -1,
							'orderby'   => 'date',
							'order' => 'DESC',
							'tax_query' => array(
							array(
							'taxonomy' => 'events',
							'field'    => 'slug',
							'terms'    => $event_cat,
							),
							),
							);
							$loop = new WP_Query( $args );
							if ( $loop->have_posts() ) {
							while ( $loop->have_posts() ) : $loop->the_post();
							get_template_part( 'template-parts/content', 'events' );
							endwhile;
							} else {
							echo __( 'No products found' );
							}
							wp_reset_postdata();
							
							
							?>
						</div>
					</div>
				</div>
				<?php    }
				}
				?>
			</div>
		</div>
	</div>
	</main><!-- #main -->
	</div><!-- #primary -->
	<?php
	get_footer();