<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package No_Dig
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">


	<?php 

             $detect = new Mobile_Detect;
             $deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');
             $scriptVersion = $detect->getScriptVersion();

			 if ( $detect->isMobile() && !$detect->isTablet() ) {
                get_template_part('template-parts/partials/content', 'footermobile');
             } else {
             	get_template_part('template-parts/partials/content', 'footerdesktop');
             }

			 

	?>

	</footer><!-- #colophon -->
</div><!-- #page -->

<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title" id="exampleModalLongTitle">Search No Dig</h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="/" method="get">
			<div class="form-group">
		    <input type="text" name="s" id="search" value="<?php the_search_query(); ?>" class="form-control" placeholder="Serach"/>
		    <br />
		    <button type="submit" class="btn btn-primary form-control">Search</button>
		    </div>
		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<?php wp_footer(); ?>

</body>
</html>
