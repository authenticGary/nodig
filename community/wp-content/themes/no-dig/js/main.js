$( document ).ready(function() {
	
$( ".menu-toggle" ).click(function() {
$( ".main-navigation" ).toggleClass( 'toggled' );
});

//Add class to menu container

$(".menu-menu-1-container").addClass('d-flex');


$(".as-post-section .card:first-child").addClass('twohundred');


// Mobile Menu

$('li.menu-item-has-children').on('click', function (e) {
        $(this).find('ul.sub-menu').toggleClass('animate');
    });

//Stickey Sidebar

$(window).scroll(function() {
if ($(window).scrollTop() > 1000) {
$('.stikey_sidebar, .checkout_stikey_sidebar').addClass('fixed_note', 1000 );
} else {
$('.stikey_sidebar, .checkout_stikey_sidebar').removeClass('fixed_note', 1000 );
}
});

//Slider Header

$('.carousel-item:first').addClass('active');
$('.carousel-indicators-numbers li:first-child').addClass('active');

$('.carousel_user_image:first').addClass('active');
$('.testimonial-indicators-numbers li:first-child').addClass('active');

//carousel_user_image

//Owl Carousel

$(".owl-carousel").owlCarousel({
	items: 6,
	loop: true,
	center: true,
  margin: 50,
  nav: true,
  navSpeed: 450,
	autoplay: true,
	autoWidth: true,
	smartSpeed: 450,
	responsive:{	
                480:{
                items:1,
                center: false,
                 },

               660:{
               items:1,
               center: false,
                },

               960:{
               items:1,
               center: false,
              }
             }
});

setTimeout(
  function() 
  {
    $( ".wdi_photo_wrap" ).append( "<i class='fab fa-instagram'></i>").fadeIn("slow");
  }, 5000);

// Select all links with hashes
$('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
      && 
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
  });



    //Woocomerce reviews 
    $('.description_tab').removeClass('active');
    $('.reviews_tab').addClass('active');
    $('#tab-reviews').attr('tabindex', '-1');
    $('#tab-reviews').css('display', 'block');



// WooCommerce Coupon Toggle

$('.as-checkout-page .as-coupon-row').hide();

$( ".showcoupon" ).click(function() {
  $( ".as-checkout-page .form-row.form-row-first" ).slideToggle( "slow", function() {});
});

// Gallery Hover
// $(".popup").append(function() {
//   return '<div class="hover"><i class="fas fa-search"></i></div>';
// });

// Cart Header
$( ".as-dropdown-cart" ).hide();
$( ".as-dropdown-toggle" ).click(function() {
  $( ".as-dropdown-cart" ).slideToggle( "slow", function() {
    // Animation complete.
  });
});

$('.as-opened').click(function() {
  $('.main-navigation').addClass('slideInRight');
});

// Mobile menubar
  $('li.menu-item-has-children').addClass('childopen');
  $(this).parent().click(function(){
   $('.sub-menu').addClass('open');
   $('.sub-menu.open').slideToggle();

});


});
