// Get the modal
$(document).ready(function () {

	var modalImg = document.getElementById("img01");
	var captionText = document.getElementById("caption");

    $('.my-modal-image').click(function () {	

    	$('#myModal').addClass("go-full");
	    modalImg.src = this.src;
	    captionText.innerHTML = this.alt;
	
	});

	$('.close').click(function () {	
	    $('#myModal').removeClass("go-full");
	});

	$('.closeLightbox').click(function () {	
	    $('#myLightbox').removeClass("go-full");
	});

	var slideIndex = 0;
	showSlides(slideIndex);

	var lightboxImg = document.getElementById('mainLightboxImage');

	var images = $('.thumbnail').children('img').map(function(){
	    return $(this).attr('src')
	}).get()

	var lastImageIndex = images.length-1 ;

	function plusSlides(n) {
	  	slideIndex += n;
		lightboxImg.src = images[slideIndex];
	}

	$('.next').click(function (event) {

		if ( slideIndex == lastImageIndex ) {
			slideIndex = 0;
		} else {
			slideIndex ++;
		}
		lightboxImg.src = images[slideIndex];
	});	

	$('.prev').click(function (event) {
		if ( slideIndex == 0 ) {
			slideIndex = lastImageIndex;
		} else {

		slideIndex --;

		}
		lightboxImg.src = images[slideIndex];

	});	


    $('.my-lightbox-image').click(function (event) {
    	event.preventDefault();
    	lightboxImg.src = this.src;
    	 slideIndex = $('.my-lightbox-image').index(this);
    	$('#myLightbox').addClass("go-full");

    });	

    $('.lightbox-thumbnail').click(function (event) {
    	event.preventDefault();
    	lightboxImg.src = this.src;
    });


	function showSlides(n) {
	  var i;
	  var slides = document.getElementsByClassName("mySlides");
	  var thumbs = document.getElementsByClassName("lightbox-thumbnail");

	  if (n > slides.length) {slideIndex = 1}
	  if (n < 1) {slideIndex = slides.length}
	  for (i = 0; i < slides.length; i++) {
	      slides[i].style.display = "none";
	  }
	  for (i = 0; i < thumbs.length; i++) {
	      thumbs[i].className = thumbs[i].className.replace(" active", "");
	  }
	  //console.log(slides, 'slides');
	  if (slides.length !== 0) {
		slides[slideIndex-1].style.display = "block";
	  	thumbs[slideIndex-1].className += " active";
	  	captionText.innerHTML = thumbs[slideIndex-1].alt;
	  } else {
	  	//console.log('no modal!');
	  }

	}


 });
