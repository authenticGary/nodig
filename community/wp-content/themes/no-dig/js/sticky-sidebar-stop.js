( function( $ ) {

var fadeFlag = false;

$(window).scroll(function(e) {

	//var distance = ($(window).height() + $("html").scrollTop());
	//var documentSize = ($(document).height()-1);

	//console.log('distance', distance);
	//console.log('document', documentSize);


  // Check if we reached bottom of the document and fadeOut the target element
  if( $(window).height() + $("html").scrollTop() >= $(document).height()-140) {

  		//console.log('fadeOut');
  		if(fadeFlag == true && $('#sticky-sidebar').hasClass("stop")) {
  			// do nothing
  		} else {
	      	$('#sticky-sidebar').addClass("stop");
      		fadeFlag = true;
  		}


  } else {

  		//console.log('fadeIn');

      // Here you can do fadeIn
      if(fadeFlag) $('#sticky-sidebar').removeClass("stop");

      fadeFlag = false;
  }
});

} )( jQuery );