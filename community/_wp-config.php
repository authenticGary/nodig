<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'nearlyli_ndforum');

/** MySQL database username */
define('DB_USER', 'nearlyli_ndforum');

/** MySQL database password */
define('DB_PASSWORD', 'authenticstyle');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '5xrTX[Z)x9:=_R)<I1{R402c7(i>w.F.g@R(2Fz[Z^:|b5h>jrnnT~VAC20[b^jZ');
define('SECURE_AUTH_KEY',  'g6!8(in*?~C_l/H+Z2RPA:/Qq(scI$={/PR_LT.F2TlH%=c,N;RLbyKJTDv0%v|G');
define('LOGGED_IN_KEY',    '{i{|w3=dk(=?S|XzK#WJA06?=xsSoZ#g#?|7$Jc,@9IU,fDKo[k}D:0q(O,j },c');
define('NONCE_KEY',        'F}$;>H<5c3hrz1c)^q,*=Fm>pzuA]|{b3RI&:VPlVGP>:6EIaay*=E0m*mio2cke');
define('AUTH_SALT',        'HhQl#ft5 8Kh2soa>Tv#]zEMkIs!~LM;[WKZzs.AS2SWw #sKbL*#3{_h_ymZ)BL');
define('SECURE_AUTH_SALT', ',?Rndx<&eqI;*ppwO-9A{nvaRp|M^/]6C3I:VYV.9(,#G{B{T`QkPcq/&xB.mEvO');
define('LOGGED_IN_SALT',   'x&,)Z@PGcHk,sik]Q{/q,0IQ]II?;inCbPE&-;9^jQTivh|*rJ@IX_)UY*ei@,no');
define('NONCE_SALT',       '!idvy|gvvCPf 8_/ 3O9EE9/JZ%m;8~#[WI4Lqny.2W,]ceY4uK{|hgFEUYMS8b;');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
